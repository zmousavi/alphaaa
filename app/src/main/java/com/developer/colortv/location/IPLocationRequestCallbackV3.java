package com.developer.colortv.location;


public interface IPLocationRequestCallbackV3 {
    /**
     * On location result.
     *
     * @param location the location
     */
    void onLocationResult(IPLocationV3 location);

    /**
     * On failed request.
     *
     * @param result the result
     */
    void onFailedRequest(String result);
}

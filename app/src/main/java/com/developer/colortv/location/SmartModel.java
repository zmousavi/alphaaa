package com.developer.colortv.location;


public class SmartModel {
    /**
     * The Ip location v 1.
     */
    public IPLocation ipLocationV1;
    /**
     * The Ip location v 2.
     */
    public IPLocationV2 ipLocationV2;
    /**
     * The Ip location v 3.
     */
    public IPLocationV3 ipLocationV3;
}

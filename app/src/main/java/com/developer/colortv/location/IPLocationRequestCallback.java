package com.developer.colortv.location;


public interface IPLocationRequestCallback {
    /**
     * On location result.
     *
     * @param location the location
     */
    void onLocationResult(IPLocation location);

    /**
     * On failed request.
     *
     * @param result the result
     */
    void onFailedRequest(String result);
}

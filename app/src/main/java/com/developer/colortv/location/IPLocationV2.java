package com.developer.colortv.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Ip location v 2.
 */
public class IPLocationV2 {

    /**
     * The Country code.
     */
    @SerializedName("country_code")
    @Expose
    public String countryCode;
    /**
     * The Country name.
     */
    @SerializedName("country_name")
    @Expose
    public String countryName;
    /**
     * The City.
     */
    @SerializedName("city")
    @Expose
    public Object city;
    /**
     * The Postal.
     */
    @SerializedName("postal")
    @Expose
    public Object postal;
    /**
     * The Latitude.
     */
    @SerializedName("latitude")
    @Expose
    public Double latitude;
    /**
     * The Longitude.
     */
    @SerializedName("longitude")
    @Expose
    public Double longitude;
    /**
     * The Pv 4.
     */
    @SerializedName("IPv4")
    @Expose
    public String iPv4;
    /**
     * The State.
     */
    @SerializedName("state")
    @Expose
    public Object state;
}

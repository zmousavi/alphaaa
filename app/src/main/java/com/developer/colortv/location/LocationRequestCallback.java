package com.developer.colortv.location;


public interface LocationRequestCallback {

    /**
     * On location result.
     * در صورت دریافت نتیجه مناسب آنرا برگشت میدهد
     *
     * @param location the SmartModel location
     */
    void onLocationResult(SmartModel location);

    /**
     * On failed request.
     *
     * @param result the String result
     */
    void onFailedRequest(String result);


}

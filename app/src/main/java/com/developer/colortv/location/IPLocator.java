package com.developer.colortv.location;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class IPLocator {
    /**
            * The constant _locationfindProcessFinished.
     */
    private static volatile boolean _locationfindProcessFinished = false;
    /**
     * The constant _appOpened.
     */
    private static volatile boolean _appOpened = false;
    /**
     * The Listener.
     */
    private IPLocationRequestCallback _listener;
    /**
     * The Context.
     */
    private Context _context;

    /**
     * Find location.
     *
     * @param context  the context
     * @param listener the listener
     */
    public void findLocation(Context context,final IPLocationRequestCallback listener) {
        Log.e("hhh", "findLocation: ");
        _listener = listener;
        _context = context;
        try {
            verTask = new IPLocator.SimpleHttpTask(_context);
            verTask.execute();
        } catch (Exception c) {
            _listener.onFailedRequest("Excetion");
        }
    }


    /**
     * The Wifi ip.
     */
    String wifiIp = "";
    /**
     * The Ver task.
     */
    SimpleHttpTask verTask;
    /**
     * The Ver task 2.
     */
    SimpleHttpTask2 verTask2;
    /**
     * The Ver task 3.
     */
    SimpleHttpTask3 verTask3;

    /**
     * The type Simple http task.
     */
    private class SimpleHttpTask extends AsyncTask<Void, Void, Void> {
        /**
         * The Context.
         */
        private final Context context;
        /**
         * The Ext ip.
         */
// android IP
        String extIP;

        /**
         * Instantiates a new Simple http task.
         *
         * @param context the context
         */
        public SimpleHttpTask(Context context) {
            this.context = context;
        }

        /**
         * On pre execute.
         */
        @Override
        protected void onPreExecute() {
        }

        /**
         * Do in background void.
         *
         * @param params the params
         * @return the void
         */
        @Override
        protected Void doInBackground(Void... params) {
            extIP = getCurrentIP(context);
            return null;
        }

        /**
         * On post execute.
         *
         * @param arg the arg
         */
        @Override
        protected void onPostExecute(Void arg) {
            wifiIp = extIP;
            if (TextUtils.isEmpty(wifiIp)) {
                verTask2 = new SimpleHttpTask2(context);
                verTask2.execute();
            } else {
                saveshopBasedIP();
            }

        }

    }

    /**
     * Saveshop based ip.
     */
    private void saveshopBasedIP() {
        if (!TextUtils.isEmpty(wifiIp)) {
            Log.e("ip", "onCreate: " + wifiIp);
            SendIPLocation _ip = new SendIPLocation();
            _ip.ip = wifiIp;

            verTask3 = new SimpleHttpTask3(_context);
            verTask3.execute();
        } else {
            _listener.onFailedRequest("ip not found");
            //ip not found so propebly he is in iran
            _locationfindProcessFinished = true;

        }

    }

    /**
     * The type Simple http task 2.
     */
    private class SimpleHttpTask2 extends AsyncTask<Void, Void, Void> {
        /**
         * The Context.
         */
        private final Context context;
        /**
         * The Ext ip.
         */
// android IP
        String extIP;

        /**
         * Instantiates a new Simple http task 2.
         *
         * @param context the context
         */
        public SimpleHttpTask2(Context context) {
            this.context = context;
        }

        /**
         * On pre execute.
         */
        @Override
        protected void onPreExecute() {
        }

        /**
         * Do in background void.
         *
         * @param params the params
         * @return the void
         */
        @Override
        protected Void doInBackground(Void... params) {
            extIP = getCurrentIP2(context);
            return null;
        }

        /**
         * On post execute.
         *
         * @param arg the arg
         */
        @Override
        protected void onPostExecute(Void arg) {
            wifiIp = extIP;
            saveshopBasedIP();
        }

    }

    /**
     * The type Simple http task 3.
     */
    private class SimpleHttpTask3 extends AsyncTask<Void, Void, Void> {
        /**
         * The Context.
         */
        private final Context context;


        /**
         * Instantiates a new Simple http task 3.
         *
         * @param context the context
         */
        public SimpleHttpTask3(Context context) {
            this.context = context;
        }

        /**
         * On pre execute.
         */
        @Override
        protected void onPreExecute() {
        }

        /**
         * Do in background void.
         *
         * @param params the params
         * @return the void
         */
        @Override
        protected Void doInBackground(Void... params) {
            getLocation(context);
            return null;
        }

        /**
         * On post execute.
         *
         * @param arg the arg
         */
        @Override
        protected void onPostExecute(Void arg) {
            _locationfindProcessFinished = true;
        }

    }

    /**
     * Convert stream to string string.
     *
     * @param is the is
     * @return the string
     */
    static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    /**
     * Gets location.
     *
     * @param context the context
     */
    private void getLocation(Context context) {
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://iplocation.com/");

// Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<>(1);
            params.add(new BasicNameValuePair("ip", wifiIp));
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

//Execute and get the response.
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    String theString = convertStreamToString(instream);
                    if (!TextUtils.isEmpty(theString)) {
                        Gson gg = new Gson();
                        IPLocation locat = gg.fromJson(theString, IPLocation.class);
                        if (locat != null ) {
                            _listener.onLocationResult(locat);
                        } else {
                            _listener.onFailedRequest("Location service not response");
                        }
                    }

                    // do something useful
                } finally {
                    instream.close();
                }
            } else {
                _listener.onFailedRequest("Location service not response");
            }
        } catch (Exception c) {
            _listener.onFailedRequest("Exception in Location service");
        }
    }


    /**
     * Gets current ip 2.
     *
     * @param context the context
     * @return the current ip 2
     */
    private String getCurrentIP2(Context context) {
        String useurl = "http://wtfismyip.com/text";

        try {
            URL ipify = new URL(useurl);
            URLConnection conn = ipify.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String ip = null;
            ip = in.readLine();
            in.close();

            if (ip != null) {
                long len = ip.length();
                if (len != -1 && len < 1024) {
                    String str = ip;
                    return (str);
                } else {
                    return "";
                }
            } else {
                return "";
            }

        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Gets current ip.
     *
     * @param context the context
     * @return the current ip
     */
    private String getCurrentIP(Context context) {
        try {
            boolean useHttps = false;

            URL ipify = useHttps ? new URL("https://api.ipify.org") : new URL("http://api.ipify.org");
            URLConnection conn = ipify.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String ip = null;
            ip = in.readLine();
            in.close();
            return ip;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}

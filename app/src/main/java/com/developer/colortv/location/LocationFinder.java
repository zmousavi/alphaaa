package com.developer.colortv.location;

import android.app.Activity;
import android.content.Context;

import java.util.PriorityQueue;
import java.util.Queue;


public class LocationFinder {
    /**
     * The Orders keep the order of method like that, first you request applanguage and after that ip version 1.
     */
    static Queue<String> orders;
    /**
     * The Ctx is Context and it used for some method becuase some of them use this for get permssion state
     */
    static Context _ctx;
    /**
     * The Act is activity and use for show dialog because some method show gps dialog or permission dialog
     */
    static Activity _act;
    /**
     * The SmartModel is smart model and grab all data
     */
    SmartModel _model = null;
    /**
     * The Listener is A callback to communicate the final result of all method
     */
    static LocationRequestCallback _listener;

    /**
     * The type Smart location finder builder is a builder that use the fluent object creation method that you can
     * use it for create smart function.
     * این کلاس برای پیاده سازی منطق fluent object creation استفاده میشود به این شکل برای هوشمند کردن کد به شکلی که کاربر هر نوع درخواستی داشت را بتواند استفاده کند پیاده سازی شده است.
     * تصور کنید که شما فقط به لیست برنامه های شخص احتیاج دارید و سایر قابلیت ها را نیاز ندارید برای اینکار یا باید کلی متد تعریف شود که هر متد کار خاصی را انجام میدهد یا میتواند به صورت زیر پیاده سازی شود که فقط با فراخوانی متد installApp این ویژگی اضافه میشود.
     */
    public static class LocationFinderBuilder {

        /**
         * The Managed instance.
         */
/*
        یک شی از کلاس پدر ایجاد میشود که زمانی که تنظیمات پیاده سازی برگشت داده شود و این شی در متند build برگشت داده میشود
         */
        private LocationFinder managedInstance = new LocationFinder();

        /**
         * Instantiates a new Smart location finder builder.
         *
         * @param ctx      the ctx
         * @param act      the act
         * @param listener the listener
         */
        public LocationFinderBuilder(Context ctx, Activity act, LocationRequestCallback listener) {
            _ctx = ctx;
            _act = act;
            _listener = listener;
            orders = new PriorityQueue<>();
        }

        /**
         * Install app smart location finder builder.
         * این متد مشخص میکند که شما میخواهید لیست برنامه های کاربر را داشته باشید و با اضافه کردن این متد به صورت خودکار لیست برنامه ها دریافت میشود
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder installApp() {
            orders.add("installApp");
            return this;
        }

        /**
         * Install persian app smart location finder builder.
         * این متد مشخص میکند که شما لیست برنامه های کاربر که در اسم ها از کلمات فارسی استفاده شده است را دریافت میکند
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder installPersianApp() {
            orders.add("installPersianApp");
            return this;
        }

        /**
         * Geo location smart location finder builder.
         * این متد مشخص میکند که شما میخواهید موقعیت کاربر را با استفاده از gps دریافت کنید.
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder geoLocation() {
            orders.add("geoLocation");
            return this;
        }

        /**
         * Ipv 1 smart location finder builder.
         * این متد مشخص میکند که شما موقعیت کاربر را با استفاده از متد ip شماره یک میخواهید
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder ipv1() {
            orders.add("ipv1");
            return this;
        }

        /**
         * Ipv 2 smart location finder builder.
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder ipv2() {
            orders.add("ipv2");
            return this;
        }

        /**
         * Ipv 3 smart location finder builder.
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder ipv3() {
            orders.add("ipv3");
            return this;
        }

        /**
         * App language smart location finder builder.
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder appLanguage() {
            orders.add("appLanguage");
            return this;
        }

        /**
         * Sys language smart location finder builder.
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder sysLanguage() {
            orders.add("sysLanguage");
            return this;
        }

        /**
         * Play location smart location finder builder.
         *
         * @return the smart location finder builder
         */
        public LocationFinder.LocationFinderBuilder playLocation() {
            orders.add("playLocation");
            return this;
        }

        /**
         * Build smart location finder.
         * این متد در نهایت امر اضافه می شود که یک شی از کلاس LocationFinder را برمیگرداند که تنظیمات کامل در آن ذخیره شده است.
         *
         * @return the smart location finder
         */
        public LocationFinder build() {
            return managedInstance;
        }
    }


    /**
     * Builder smart location finder builder.
     *
     * @param ctx      the ctx
     * @param act      the act
     * @param listener the listener
     * @return the smart location finder builder
     */
    public static LocationFinder.LocationFinderBuilder builder(Context ctx, Activity act, LocationRequestCallback listener) {
        return new LocationFinder.LocationFinderBuilder(ctx, act, listener);
    }


    /**
     * Start smart location finder.
     * برای شروع عملیات گرفتن موقعیت باید این تابع صدا زده شود و این تابع بر حسب اینکه شما در builder چه نوع متدی را انتخاب کرده اید، انرا اجرا و نتیجه را برمیگرداند
     *
     * @return the smart location finder
     */
    public LocationFinder Start() {
        if (orders != null && orders.size() > 0)
            _model = new SmartModel();
        Router();
        return this;
    }

    /**
     * Router smart location finder.
     * دقیقا همانند یک روتر عمل میکند به این شکل که فرض کنید ما لیست برنامه های کاربر به همراه موقعیت مکانی اون با استفاده از gps را درخواست داده اید
     * این تابع با توجه به اینکه از یک متغیر از نوع صف استفاده میکند و هر بار یک ایتم از انرا پاپ میکند اولا به صورت ترتیبی عمل میکند و دوما امکان ندارد که تکرار داشته باشد و سوما که هر ایتم که تمام شد به سراغ مورد بعدی میرود که باعث میشود ترتیب رعایت شود
     *
     * @return the smart location finder
     */
    private void Router() {
        if (orders != null && orders.size() > 0) {
            switch (orders.poll()) {
                case "ipv1":
                    getIPV1();
                    break;
                case "ipv2":
                    getIPV2();
                    break;
                case "ipv3":
                    getIPV3();
                    break;
            }
        } else {
            /*
            با توجه به این که اگر کاربر هیچ متدی را درخواست نداده باشد متغیر _model ایجاد نمیشود پس در صورت پر بودن یعنی نتیجه ای دریافت شده است پس میتواند برگشت داده شود
             */

            if (_model != null) {
                _listener.onLocationResult(_model);
            } else {
                _listener.onFailedRequest("خطا");
            }
        }
    }


    /**
     * Gets ipv 1.
     */
    private void getIPV1() {
        IPLocator ip = new IPLocator();
        ip.findLocation(_ctx, new IPLocationRequestCallback() {
            @Override
            public void onLocationResult(IPLocation location) {
                if (location != null) {
                    _model.ipLocationV1 = location;
                }
                Router();
            }

            @Override
            public void onFailedRequest(String result) {
                Router();
            }
        });
    }

    /**
     * Gets ipv 2.
     */
    private void getIPV2() {
        IPLocatorV2 ipv2 = new IPLocatorV2();
        ipv2.findLocation(_ctx, new IPLocationRequestCallbackV2() {
            @Override
            public void onLocationResult(IPLocationV2 location) {
                if (location != null) {
                    _model.ipLocationV2 = location;
                }
                Router();
            }

            @Override
            public void onFailedRequest(String result) {
                Router();
            }
        });
    }

    /**
     * Gets ipv 3.
     */
    private void getIPV3() {
        IPLocatorV3 ipv3 = new IPLocatorV3();
        ipv3.findLocation(_ctx, new IPLocationRequestCallbackV3() {
            @Override
            public void onLocationResult(IPLocationV3 location) {
                if (location != null) {
                    _model.ipLocationV3 = location;
                }
                Router();
            }

            @Override
            public void onFailedRequest(String result) {
                Router();
            }
        });
    }



}

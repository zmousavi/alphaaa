package com.developer.colortv.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Ip location.
 */
public class IPLocation {

    /**
     * The Area code.
     */
    @SerializedName("area_code")
    @Expose
    public Integer areaCode;
    /**
     * The City.
     */
    @SerializedName("city")
    @Expose
    public String city;
    /**
     * The Company.
     */
    @SerializedName("company")
    @Expose
    public String company;
    /**
     * The Continent code.
     */
    @SerializedName("continent_code")
    @Expose
    public String continentCode;
    /**
     * The Country code.
     */
    @SerializedName("country_code")
    @Expose
    public String countryCode;
    /**
     * The Country code 3.
     */
    @SerializedName("country_code3")
    @Expose
    public String countryCode3;
    /**
     * The Country name.
     */
    @SerializedName("country_name")
    public String countryName;
    /**
     * The Found.
     */
    @SerializedName("found")
    @Expose
    public Integer found;
    /**
     * The Ip.
     */
    @SerializedName("ip")
    @Expose
    public String ip;
    /**
     * The Ip header.
     */
    @SerializedName("ip_header")
    @Expose
    public String ipHeader;
    /**
     * The Lat.
     */
    @SerializedName("lat")
    @Expose
    public Double lat;
    /**
     * The Lng.
     */
    @SerializedName("lng")
    @Expose
    public Double lng;
    /**
     * The Metro code.
     */
    @SerializedName("metro_code")
    @Expose
    public Integer metroCode;
    /**
     * The Postal code.
     */
    @SerializedName("postal_code")
    @Expose
    public Object postalCode;
    /**
     * The Region.
     */
    @SerializedName("region")
    @Expose
    public Object region;
    /**
     * The Region name.
     */
    @SerializedName("region_name")
    @Expose
    public Object regionName;
    /**
     * The Time zone.
     */
    @SerializedName("time_zone")
    @Expose
    public String timeZone;
}

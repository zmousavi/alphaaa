package com.developer.colortv.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Ip location v 3.
 */
public class IPLocationV3 {

    /**
     * The As.
     */
    @SerializedName("as")
    @Expose
    public String as;
    /**
     * The City.
     */
    @SerializedName("city")
    @Expose
    public String city;
    /**
     * The Country.
     */
    @SerializedName("country")
    @Expose
    public String country;
    /**
     * The Country code.
     */
    @SerializedName("countryCode")
    @Expose
    public String countryCode;
    /**
     * The Isp.
     */
    @SerializedName("isp")
    @Expose
    public String isp;
    /**
     * The Lat.
     */
    @SerializedName("lat")
    @Expose
    public Double lat;
    /**
     * The Lon.
     */
    @SerializedName("lon")
    @Expose
    public Double lon;
    /**
     * The Mobile.
     */
    @SerializedName("mobile")
    @Expose
    public Boolean mobile;
    /**
     * The Org.
     */
    @SerializedName("org")
    @Expose
    public String org;
    /**
     * The Proxy.
     */
    @SerializedName("proxy")
    @Expose
    public Boolean proxy;
    /**
     * The Query.
     */
    @SerializedName("query")
    @Expose
    public String query;
    /**
     * The Region.
     */
    @SerializedName("region")
    @Expose
    public String region;
    /**
     * The Region name.
     */
    @SerializedName("regionName")
    @Expose
    public String regionName;
    /**
     * The Reverse.
     */
    @SerializedName("reverse")
    @Expose
    public String reverse;
    /**
     * The Status.
     */
    @SerializedName("status")
    @Expose
    public String status;
    /**
     * The Timezone.
     */
    @SerializedName("timezone")
    @Expose
    public String timezone;
    /**
     * The Zip.
     */
    @SerializedName("zip")
    @Expose
    public String zip;
}

package com.developer.colortv.location;


public interface IPLocationRequestCallbackV2 {
    /**
     * On location result.
     *
     * @param location the location
     */
    void onLocationResult(IPLocationV2 location);

    /**
     * On failed request.
     *
     * @param result the result
     */
    void onFailedRequest(String result);
}

package com.developer.colortv.location;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;


public class IPLocatorV3 {
    /**
     * The constant _appOpened.
     */
    private static volatile boolean _appOpened = false;
    /**
     * The Listener.
     */
    private IPLocationRequestCallbackV3 _listener;

    /**
     * Find location.
     *
     * @param context  the context
     * @param listener the listener
     */
    public void findLocation(Context context, final IPLocationRequestCallbackV3 listener) {
        Log.e("hhh", "findLocation: ");
        _listener = listener;
        Context _context = context;
        try {
            verTask3 = new SimpleHttpTask3(_context);
            verTask3.execute();
        } catch (Exception c) {
            _listener.onFailedRequest("Excetion");
        }
    }


    /**
     * The Ver task 3.
     */
    SimpleHttpTask3 verTask3;


    /**
     * The type Simple http task 3.
     */
    private class SimpleHttpTask3 extends AsyncTask<Void, Void, Void> {
        /**
         * The Context.
         */
        private final Context context;


        /**
         * Instantiates a new Simple http task 3.
         *
         * @param context the context
         */
        public SimpleHttpTask3(Context context) {
            this.context = context;
        }

        /**
         * On pre execute.
         */
        @Override
        protected void onPreExecute() {
        }

        /**
         * Do in background void.
         *
         * @param params the params
         * @return the void
         */
        @Override
        protected Void doInBackground(Void... params) {
            getLocation(context);
            return null;
        }

        /**
         * On post execute.
         *
         * @param arg the arg
         */
        @Override
        protected void onPostExecute(Void arg) {
            boolean _locationfindProcessFinished = true;
        }

    }

    /**
     * Convert stream to string string.
     *
     * @param is the is
     * @return the string
     */
    static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    /**
     * Gets location.
     *
     * @param context the context
     */
    private void getLocation(Context context) {
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet("http://ip-api.com/json/?fields=country,countryCode,region,regionName,city,zip,lat,lon,timezone,isp,org,as,reverse,mobile,proxy,query,status,message");

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    String theString = convertStreamToString(instream);
                    if (!TextUtils.isEmpty(theString)) {
                        Gson gg = new Gson();
                        IPLocationV3 locat = gg.fromJson(theString, IPLocationV3.class);
                        if (locat != null) {
                            _listener.onLocationResult(locat);
                        } else {
                            _listener.onFailedRequest("Location service not response");
                        }
                    }

                    // do something useful
                } finally {
                    instream.close();
                }
            } else {
                _listener.onFailedRequest("Location service not response");
            }
        } catch (Exception c) {
            _listener.onFailedRequest("Exception in Location service");
        }
    }


}

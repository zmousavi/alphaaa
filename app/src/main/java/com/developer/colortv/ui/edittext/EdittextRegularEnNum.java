package com.developer.colortv.ui.edittext;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

public class EdittextRegularEnNum extends AppCompatAutoCompleteTextView {

    public EdittextRegularEnNum(Context context) {
        super(context);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN SansMobile(NoEn).ttf"));
    }

    public EdittextRegularEnNum(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN SansMobile(NoEn).ttf"));
    }

    public EdittextRegularEnNum(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN SansMobile(NoEn).ttf"));
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        super.performFiltering(text, keyCode);
    }
}

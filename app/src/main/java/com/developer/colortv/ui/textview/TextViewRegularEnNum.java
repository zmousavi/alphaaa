package com.developer.colortv.ui.textview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class TextViewRegularEnNum extends TextView {
    public TextViewRegularEnNum(Context context) {
        super(context);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN SansMobile(NoEn).ttf"));
    }

    public TextViewRegularEnNum(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN SansMobile(NoEn).ttf"));
    }

    public TextViewRegularEnNum(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRAN SansMobile(NoEn).ttf"));
    }
}

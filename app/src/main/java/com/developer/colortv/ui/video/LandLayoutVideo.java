package com.developer.colortv.ui.video;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.developer.colortv.R;
import com.shuyu.gsyvideoplayer.utils.Debuger;

/**
 * Created by shuyu on 2016/12/23.
 * CustomGSYVideoPlayer是试验中，建议使用的时候使用StandardGSYVideoPlayer
 */
public class LandLayoutVideo extends FullscreenVideoPlayer {

    private boolean isLinkScroll = false;

    /**
     * 1.5.0开始加入，如果需要不同布局区分功能，需要重载
     */
    public LandLayoutVideo(Context context) {
        super(context);
    }

    public LandLayoutVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void init(Context context) {
        super.init(context);
        post(new Runnable() {
            @Override
            public void run() {
                gestureDetector = new GestureDetector(getContext().getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onDoubleTap(MotionEvent e) {
                        touchDoubleUp();
                        return super.onDoubleTap(e);
                    }

                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent e) {
                        if (!mChangePosition && !mChangeVolume && !mBrightness) {
                            onClickUiToggle();
                        }
                        Debuger.printfError("555a","9999999999999999999999");
                        return super.onSingleTapConfirmed(e);
                    }

                    @Override
                    public void onLongPress(MotionEvent e) {
                        super.onLongPress(e);
                        Debuger.printfError("555a","0000000000000000000000");
                    }
                });
            }
        });
    }

    //这个必须配置最上面的构造才能生效
    @Override
    public int getLayoutId() {
//        if (mIfCurrentIsFullscreen) {
            return R.layout.control_view_landscap;
//        }
//        return R.layout.control_view_portrait2;
    }

//    @Override
//    protected void updateStartImage() {
//        if (mIfCurrentIsFullscreen) {
//            if(mStartButton instanceof  ImageView) {
//                ImageView imageView = (ImageView) findViewById(R.id.btnPlayTv);
//                if (mCurrentState == CURRENT_STATE_PLAYING) {
//                    imageView.setImageResource(R.drawable.icons_8_pause);
//                } else if (mCurrentState == CURRENT_STATE_ERROR) {
//                    imageView.setImageResource(R.drawable.ic_play);
//                } else {
//                    imageView.setImageResource(R.drawable.ic_play);
//                }
//            }
//        } else {
//            super.updateStartImage();
//        }
//    }

//    @Override
//    public int getEnlargeImageRes() {
//            return R.drawable.ic_full_screen;
//    }
//
//    @Override
//    public int getShrinkImageRes() {
//        return R.drawable.ic_minscreen;
//    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isLinkScroll && !isIfCurrentIsFullscreen()) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.onInterceptTouchEvent(ev);
    }


    @Override
    protected void resolveNormalVideoShow(View oldF, ViewGroup vp, FullscreenVideoPlayer gsyVideoPlayer) {
        LandLayoutVideo landLayoutVideo = (LandLayoutVideo)gsyVideoPlayer;
        landLayoutVideo.dismissProgressDialog();
        landLayoutVideo.dismissVolumeDialog();
        landLayoutVideo.dismissBrightnessDialog();
        super.resolveNormalVideoShow(oldF, vp, gsyVideoPlayer);
    }

    public void setLinkScroll(boolean linkScroll) {
        isLinkScroll = linkScroll;
    }
}

package com.developer.colortv.ui.button;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;

public class ButtonMedium extends AppCompatButton {
    public ButtonMedium(Context context) {
        super(context);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Medium.ttf"));
    }

    public ButtonMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Medium.ttf"));
    }

    public ButtonMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Medium.ttf"));
    }
}

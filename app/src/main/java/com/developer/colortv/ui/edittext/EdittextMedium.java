package com.developer.colortv.ui.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

public class EdittextMedium extends AppCompatAutoCompleteTextView {
    public EdittextMedium(Context context) {
        super(context);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Medium.ttf"));
    }

    public EdittextMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Medium.ttf"));
    }

    public EdittextMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Medium.ttf"));
    }
}

package com.developer.colortv.ui.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.developer.colortv.R;

import androidx.appcompat.app.AppCompatActivity;


public class CustomDialogClass extends Dialog implements
        View.OnClickListener {

    public AppCompatActivity c;
    public Dialog d;
    public Button yes;
    int _state=0;
    String _url="";


    public CustomDialogClass(AppCompatActivity a, String url, int state) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        _url=url;
        _state=state;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_connection);


        getWindow().setLayout(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View v = getWindow().getDecorView();
        v.setBackgroundResource(android.R.color.transparent);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(null);
        yes = (Button) findViewById(R.id.btnConnection);
        yes.setOnClickListener(this);

//        if (_state==1)
//        {
//            setContentView(R.layout.dialog_update);
//            com.example.guojunjie.shadowdemo.util.Views.IranSansTextView  txtUpdate = (com.example.guojunjie.shadowdemo.util.Views.IranSansTextView) findViewById(R.id.txtUpdate);
//            com.example.guojunjie.shadowdemo.util.Views.IranSansTextView  txtExit = (com.example.guojunjie.shadowdemo.util.Views.IranSansTextView) findViewById(R.id.txtExit);
//
//            txtUpdate.setOnClickListener(this);
//            txtExit.setOnClickListener(this);
//        }
    }

    @Override
    public void onBackPressed() {
//                    c.moveTaskToBack(false);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
//        c.finish();
//        super.onBackPressed();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConnection:
                Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity( new Intent( intent ) );

                break;
//
//            case R.id.txtUpdate:
//                if (checkAppInstalled.isPackageInstalled("com.farsitel.bazaar", c)) {
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse(_url));
//                  c.startActivity(intent);
//
//                } else {
//                    Toast.makeText(c, "برنامه کافه بازار روی دستگاه شما نصب نیست.", Toast.LENGTH_LONG).show();
//
//                }
//
//            case R.id.txtExit:
//                c.finish();
//                c.moveTaskToBack(true);
//                android.os.Process.killProcess(android.os.Process.myPid());
//                System.exit(0);
        }
        dismiss();
    }
}

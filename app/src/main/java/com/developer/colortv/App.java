package com.developer.colortv;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.developer.colortv.utils.PrefManager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import io.fabric.sdk.android.Fabric;
import me.cheshmak.android.sdk.core.Cheshmak;

public class App extends MultiDexApplication {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    public static LayoutInflater inflater;
    public static Activity currentActivity;
    //adColony
//    AdColonyInterstitial ad;
//    AdColonyInterstitialListener listener;
//    AdColonyAdOptions adOptions;

    public static final int DOWNLOAD_BUFFER_SIZE = 8 * 1024;
    public static final String ADDRESS = "http://www.zinono.com/note";
    public static final String FORUM = "http://www.zinono.com/forum";
    private String TAG = App.class.getSimpleName() + "LOG";

    static class setNewId {

        String title;
        String singer;
        String track;
        int id;
        long longId;
    }

    public static volatile Context context;
    // Create the instance
    private static App instance;

    public static App getInstance() {
        if (instance == null) {
            synchronized (App.class) {
                if (instance == null)
                    instance = new App();
            }
        }
        // Return the instance
        return instance;
    }

    PrefManager pref;
    AdView adView;
    InterstitialAd interstitialAd;

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        //cheshmak
        Cheshmak.with(getApplicationContext());
        Cheshmak.initTracker("OkDv9HN+y5vgB0XA6vSz8w==");
//        CheshmakAds.setTestMode(true);


        Log.e(TAG, "onCreate: device name : " + getDeviceName());
        pref = new PrefManager(this);

        instance = this;
        context = getApplicationContext();

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//        setupAds(context);

        prefs = getSharedPreferences("pref", MODE_PRIVATE);
        editor = prefs.edit();
        editor.putBoolean("firstLogin", true);
        editor.putString("lastChannel", prefs.getString("lastTvChannel", "سه"));
        editor.apply();

    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void setupAds(final Context ctx) {

//        loadAdColony(ctx);

        String aa = pref.getString("app_id", "");
        // set aa to None means hide ad view
        if (!aa.contentEquals("None")) {
            MobileAds.initialize(ctx, pref.getString("app_id", ""));
            adView = new AdView(ctx);
            adView.setId(R.id.myAdsView);
            adView.setAdSize(AdSize.BANNER);

            adView.setAdUnitId(pref.getString("Banner_id", ""));
            // Request for Ads
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();

            interstitialAd = new InterstitialAd(context);
            interstitialAd.setAdUnitId(PrefManager.with(context, "prefs", MODE_PRIVATE).getString("Interstitial_id", ""));
            interstitialAd.setAdListener(new AdListener(){
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    Log.e(TAG, "onAdFailedToLoad: inters error : " + i );
                    try {
//                        ad.show();
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    setInterstitialAd(interstitialAd);
                    Log.e(TAG, "onAdLoaded: inters" );
                }
            });
            interstitialAd.loadAd(adRequest);


//
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
//                Toast.makeText(context, "loaded", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "onAdLoaded" );
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // Code to be executed when an ad request fails.
//                Toast.makeText(ctx, "banner failed " + errorCode, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "onAdFailedToLoad : " + errorCode );
                }

                @Override
                public void onAdOpened() {
                    // Code to be executed when an ad opens an overlay that
                    // covers the screen.
                    Log.e(TAG, "onAdOpened" );

                }

                @Override
                public void onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                }

                @Override
                public void onAdClosed() {
                    // Code to be executed when when the user is about to return
                    // to the app after tapping on an ad.
                }
            });


            // Load ads into Banner Ads
            adView.loadAd(adRequest);
        }
    }

    public void setInterstitialAd(InterstitialAd interstitialAd) {
        this.interstitialAd = interstitialAd;
    }
    public InterstitialAd getInterstitialAd() {
        return interstitialAd;
    }

//    private void loadAdColony(Context context) {
//        // Construct optional app options object to be sent with configure
//        AdColonyAppOptions appOptions = new AdColonyAppOptions()
//                .setKeepScreenOn(true);
//
//        // Configure AdColony in your launching Activity's onCreate() method so that cached ads can
//        // be available as soon as possible.
//        AdColony.configure(this, appOptions, PrefManager.with(context).getString("adColony_app_id", ""),
//                PrefManager.with(context).getString("adColony_zone_id", ""));
//
//        // Optional user metadata sent with the ad options in each request
//        AdColonyUserMetadata metadata = new AdColonyUserMetadata()
//                .setUserEducation(AdColonyUserMetadata.USER_EDUCATION_BACHELORS_DEGREE)
//                .setUserGender(AdColonyUserMetadata.USER_MALE);
//
//        // Ad specific options to be sent with request
//        adOptions = new AdColonyAdOptions().setUserMetadata(metadata);
//
//        // Set up listener for interstitial ad callbacks. You only need to implement the callbacks
//        // that you care about. The only required callback is onRequestFilled, as this is the only
//        // way to get an ad object.
//        listener = new AdColonyInterstitialListener() {
//            @Override
//            public void onRequestFilled(AdColonyInterstitial ad) {
//                // Ad passed back in request filled callback, ad can now be shown
//                App.this.ad = ad;
//                Log.d(TAG, "onRequestFilled");
//            }
//
//            @Override
//            public void onRequestNotFilled(AdColonyZone zone) {
//                // Ad request was not filled
//                Log.d(TAG, "onRequestNotFilled");
//            }
//
//            @Override
//            public void onOpened(AdColonyInterstitial ad) {
//                // Ad opened, reset UI to reflect state change
//                Log.d(TAG, "onOpened");
//            }
//
//            @Override
//            public void onExpiring(AdColonyInterstitial ad) {
//                // Request a new ad if ad is expiring
//                AdColony.requestInterstitial(PrefManager.with(context).getString("adColony_zone_id", ""), this, adOptions);
//                Log.d(TAG, "onExpiring");
//            }
//        };
//    }

    public void loadInterstitialAds() {
        if (interstitialAd.isLoaded()) {

//            ExoPlayerVideoHandler.getInstance().releaseVideoPlayer();
            interstitialAd.show();
        }
    }

    public void loadAd(FrameLayout layAd) {

        if (adView != null) {
            // Locate the Banner Ad in activity xml
            if (adView.getParent() != null) {
                ViewGroup tempVg = (ViewGroup) adView.getParent();
                tempVg.removeView(adView);
            }

            //remove last ad view and add new one
            View lastAdView = layAd.findViewById(R.id.myAdsView);
            if (lastAdView != null)
            {
                layAd.removeView(lastAdView);
            }

            layAd.addView(adView);
        }

    }

    public void hideBannerAds() {
        adView.setVisibility(View.GONE);
    }

}


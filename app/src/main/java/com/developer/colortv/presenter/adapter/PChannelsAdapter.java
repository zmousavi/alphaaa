package com.developer.colortv.presenter.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.utils.CircleTransform;
import com.developer.colortv.R;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.activity.FullscreenVideoActivity;
import com.developer.colortv.view.adapter.ChannelsAdapter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;
import com.squareup.picasso.Picasso;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PChannelsAdapter {
    List<Channel> channels;
    Context context;
    private String TAG = getClass().getSimpleName();

    public PChannelsAdapter(Context context, List<Channel> channels) {
        this.channels = channels;
        this.context = context;
    }


    public void onBind(ChannelsAdapter.viewHolder holder, int position) {
        if (channels.get(position).getContent().toLowerCase().contains("رادیو")) {
            try {
                Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + channels.get(position).getImgAdrs());
                Picasso.get().load(uri).transform(new CircleTransform()).placeholder(R.drawable.radio_default).into(holder.imgChannelItem);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {
                Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + channels.get(position).getImgAdrs());
                Picasso.get().load(uri).transform(new CircleTransform()).placeholder(R.drawable.satellite).into(holder.imgChannelItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            holder.txtChannelItem.setText(channels.get(position).getTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.llChannelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(channels.get(position));
            }
        });
    }

    public int getItemCount() {
        return channels.size();
    }

    private void onItemClick(Channel channel) {
//        Toast.makeText(context, channel.getTitle(), Toast.LENGTH_SHORT).show();
        trackEvent("ChannelClick", "channel name", channel.getTitle() + " - " + channel.getContent());

        if (channel.getContent().toLowerCase().contains("فیلترشکن)")) {
            if (checkVpn()) {
                Intent intent = new Intent(context, FullscreenVideoActivity.class);
                intent.putExtra("channel", channel);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else {
                context.sendBroadcast(new Intent().setAction("showVpnDialog"));
            }
        } else {
            if (channel.getContent().toLowerCase().contains("رادیو")) {
                context.sendBroadcast(new Intent().setAction("radioChannelChange").putExtra("channel", channel));
            } else {
                Intent intent = new Intent(context, FullscreenVideoActivity.class);
                intent.putExtra("channel", channel);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        }
    }

    private boolean checkVpn() {
        List<String> networkList = new ArrayList<>();
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.isUp())
                    networkList.add(networkInterface.getName());
            }
        } catch (Exception ex) {
            Log.e(TAG, "checkVpn: " + ex.getMessage());
        }

        return networkList.contains("tun0");
    }


    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }

}

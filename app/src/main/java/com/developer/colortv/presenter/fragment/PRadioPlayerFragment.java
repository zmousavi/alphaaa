package com.developer.colortv.presenter.fragment;

import android.content.Context;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.model.dao.DAO;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.fragment.RadioPlayerFragment1;
import com.developer.colortv.view.interfaces.IRadioPlayerFragment;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;

import java.util.ArrayList;
import java.util.List;

public class PRadioPlayerFragment {
    private final String TAG = getClass().getSimpleName();
    IRadioPlayerFragment fragment;
    Channel channel;
    boolean isPlaying = true;
    List<Channel> channels;
    Context context;

    public PRadioPlayerFragment(RadioPlayerFragment1 fragment) {
        this.fragment = fragment;
        context = fragment.getContext();

        channel = fragment.getArguments().getParcelable("channel");
    }


    public void loadChannelsByCat() {
        channels = new ArrayList<>();
        DAO dao = new DAO(context);
        channels = dao.getRadioChannels(channel.getSubject());
    }

    public void playRadio() {
        trackEvent("PlayRadioChannel", "channel name", channel.getTitle());
        fragment.onPlay(channel.getTitle(), channel.getMore1(), channel.getImgAdrs());
    }

    public void goToNextChannel() {
        trackEvent("GoToNextRadioChannel", "channel name", channel.getTitle());

        try {
            int pos = 0;

            for (int i = 0; i < channels.size(); i++) {
                if (channel.getTitle().equalsIgnoreCase(channels.get(i).getTitle()))
                    pos = i;
            }
            channel = channels.get(pos + 1);

            playRadio();
        } catch (Exception e) {

        }
    }

    public void gotoPreChannel() {
        trackEvent("GoToPreviousRadioChannel", "channel name", channel.getTitle());

        try {
            int pos = 0;

            for (int i = 0; i < channels.size(); i++) {
                if (channel.getTitle().equalsIgnoreCase(channels.get(i).getTitle()))
                    pos = i;
            }
            channel = channels.get(pos - 1);

            playRadio();
        } catch (Exception e) {

        }
    }


    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }
}

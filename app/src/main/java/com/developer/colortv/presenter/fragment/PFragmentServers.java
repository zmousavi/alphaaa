package com.developer.colortv.presenter.fragment;

import android.content.Context;
import android.util.Log;

import com.developer.colortv.model.dao.DAO;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.fragment.FragmentServers;
import com.developer.colortv.view.interfaces.IFragmentServers;

import java.util.List;


public class PFragmentServers {
    IFragmentServers fragmentServers;
    DAO dao;
    Context context;
    private String TAG = getClass().getSimpleName();

    public PFragmentServers(FragmentServers fragmentServers) {
        this.fragmentServers = fragmentServers;
        context = fragmentServers.getContext();
        dao = new DAO(context);
    }

    public void loadServers(String channelName) {
        Log.e(TAG, "loadServers: " + channelName);
        List<Channel> channels = dao.findChannelsByName(channelName);
        fragmentServers.onGetServers(channels);
    }
}

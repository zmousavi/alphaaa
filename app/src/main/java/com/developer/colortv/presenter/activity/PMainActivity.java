package com.developer.colortv.presenter.activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.developer.colortv.utils.PrefManager;
import com.developer.colortv.model.dao.DAO;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.presenter.adapter.PFavChannelsAdapter;
import com.developer.colortv.view.activity.MainActivity;
import com.developer.colortv.view.interfaces.IMainActivity;

import java.util.ArrayList;
import java.util.List;

import team.pnx.forceupdate.ForceListener;
import team.pnx.forceupdate.ForceUpdate;
import team.pnx.forceupdate.Response;

public class PMainActivity {
    IMainActivity activity;
    DAO dao;
    Context context;
    private String TAG = this.getClass().getSimpleName();
    Activity mainActivity;

    public PMainActivity(MainActivity activity) {
        this.activity = activity;
        dao = new DAO(activity.getApplicationContext());
        this.context = activity.getApplicationContext();
        mainActivity = activity;

    }

    public void loadFavChannels() {
        List<Channel> channels = new ArrayList<>();
        channels = dao.getFavChannels();

        PFavChannelsAdapter presenterAdapter = new PFavChannelsAdapter(channels, context);
        activity.onGetFavChannels(presenterAdapter);


        checkForUpdate();
    }

    private void checkForUpdate() {
        try {
            //Force update
            int version = 0;
            ForceUpdate forceUpdate = new ForceUpdate(mainActivity, "http://136.243.122.37:3553/");
            try {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                version = Integer.valueOf(pInfo.versionCode);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            forceUpdate.checkForce(context.getPackageName(), version, ForceUpdate.MarketName.GOOGLE, new ForceListener() {
                @Override
                public void onResponse(Response response) {
                    Log.e(TAG, "onResponse: appId : " + response.getData().getApp_id()
                    + "\nbannerId : " + response.getData().getBanner_id()
                    + "\ninterstitialId : " + response.getData().getInterstitial_id());

                    PrefManager.with(context).save("app_id", response.getData().getApp_id());
                    PrefManager.with(context).save("Banner_id", response.getData().getBanner_id());
                    PrefManager.with(context).save("Interstitial_id", response.getData().getInterstitial_id());

                }

                @Override
                public void onFail(Throwable t) {
                    Log.e(TAG, "onFail: " + t.getMessage());
                }

                @Override
                public void onAcceptClicked() {

                }

                @Override
                public void onDenyClicked() {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

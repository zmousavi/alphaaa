package com.developer.colortv.presenter.adapter;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.App;
import com.developer.colortv.utils.CircleTransform;
import com.developer.colortv.R;
import com.developer.colortv.model.dao.DAO;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.activity.FullscreenVideoActivity;
import com.developer.colortv.view.activity.SearchActivity;
import com.developer.colortv.view.adapter.SearchAdapter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PSearchAdapter {
    List<Channel> searchList;
    DAO dao;
    SearchActivity context;

    public PSearchAdapter(SearchActivity context, List<Channel> searchList) {
        this.searchList = searchList;
        dao = new DAO(App.context);
//        this.historyList = new ArrayList<>();
        this.context = context;
    }

    public int getSearchItemCount() {
        return searchList.size();
    }

//    public int getHistoryItemCount() {
//        try {
//            return historyList.size();
//        } catch (Exception e) {
//            return 0;
//        }
//    }

    public void onBindSearchItems(SearchAdapter.viewHolderSearch holder, int position) {
//        SearchAdapter.viewHolderSearch viewHolderSearch = (SearchAdapter.viewHolderSearch) viewHolder;
        Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + searchList.get(position).getImgAdrs());
        Picasso.get().load(uri).transform(new CircleTransform()).placeholder(R.drawable.satellite).into(holder.img);

        holder.txtName.setText(searchList.get(position).getTitle());
        holder.txtType.setText(searchList.get(position).getContent());

        holder.rlSearchItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                addToRecents(searchList.get(position));
                setOnItemClick(searchList.get(position));
            }
        });
    }

//    public void onBindHistoryItem(SearchAdapter.viewHolderHistory holder, int position) {
//
//        holder.txtName.setText(historyList.get(position).getTitle());
//        holder.txtType.setText(historyList.get(position).getContent());
//
//        holder.llHistoryItem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setOnItemClick(historyList.get(position));
//            }
//        });
//
//        holder.btnRemoveFromHistory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                removFromHistory(historyList.get(position), holder);
//            }
//        });
//    }


//    private void addToRecents(Channel channel) {
//        dao.addToRecents(channel);
//    }

//    private void removFromHistory(Channel channel, SearchAdapter.viewHolderHistory holder) {
//        Toast.makeText(App.context, channel.getTitle() + " removed", Toast.LENGTH_SHORT).show();
//        dao.removeFromRecents(channel);
//        historyList.remove(channel);
//        //refresh list
//        for (int i = 0; i < historyList.size(); i++) {
//            onBindHistoryItem(holder, i);
//        }
//    }


    private void setOnItemClick(Channel channel) {
        trackEvent("ChannelClickBySearch", "channel name", channel.getTitle() + " - " + channel.getContent());
        if (channel.getContent().contains("رادیو")){
            context.showRadioPlayer(channel);
        } else {
            Intent intent = new Intent(context, FullscreenVideoActivity.class);
            intent.putExtra("channel", channel);
            context.startActivity(intent);
        }
    }



    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }
}

package com.developer.colortv.presenter.fragment;

import android.content.Context;
import android.util.Log;

import com.developer.colortv.model.dao.DAO;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.presenter.adapter.PChannelsAdapter;
import com.developer.colortv.view.fragment.ChannelsFragment;
import com.developer.colortv.view.interfaces.IChannelsFragment;

import java.util.ArrayList;
import java.util.List;

public class PChannelsFragment {
    IChannelsFragment fragment;
    String type, category = "";
    DAO dao;
    Context context;
    List<Channel> channels;
    private String TAG = getClass().getSimpleName();

    public PChannelsFragment(ChannelsFragment fragment) {
        this.fragment = fragment;

        context = fragment.getContext();
        dao = new DAO(fragment.getContext());
        type = fragment.getArguments().getString("type");
        category = fragment.getArguments().getString("category");

        this.fragment.onShowCategory(category);
    }

    public void loadChannels() {
        channels = new ArrayList<>();
        switch (type) {
            case "tv":
                switch (category) {
                    case "global":
                        channels = dao.getTvGlobal();
                        break;

                    case "provincial":
                        channels = dao.getTvProvincialChannels();
                        for (Channel channel :
                                channels) {
                            Log.e(TAG, "loadChannels: " + channel.getTitle());
                        }
                        break;
                }
                break;

            case "sat":
                channels = dao.findChannelsByCat(category);
                break;

            case "radio":
                channels = dao.getRadioChannels(category);
                break;

            case "iranianTV":
                channels = dao.getIranianTVChannels();
                break;
        }

        PChannelsAdapter adapterPresenter = new PChannelsAdapter(context, channels);
        fragment.onLoadChannels(adapterPresenter);
    }
}

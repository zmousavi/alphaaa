package com.developer.colortv.presenter.activity;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.model.dao.DAO;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.activity.SearchActivity;
import com.developer.colortv.view.interfaces.ISearchActivity;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;

import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.List;

public class PSearchActivity {
    ISearchActivity activity;
    Context context;
    List<Channel> channels;
    List<Channel> recents;
    public static String TAG="sadegh";

    public PSearchActivity(SearchActivity activity) {
        this.activity = activity;
        context = activity.getApplicationContext();
    }


    public void loadChannels() {
        DAO dao = new DAO(context);
        channels = new ArrayList<>();
        channels = dao.getAllChannels();
        //online channels
        List<Channels> channelsList = new ArrayList<>();
//        channelsList.addAll(ConstantValues.GLOABAL_CHANNELS);
//        channelsList.addAll(ConstantValues.INTERNATIONAL_CHANNELS);
//        channelsList.addAll(ConstantValues.PROVINTIAL_CHANNELS);
//        for (Channels channels : channelsList)
//        {
//            Channel channel = new Channel();
//            channel.setContent("تلویزیون داخلی");
//            channel.setTitle(channels.getChannelName());
//            channel.setId(Integer.valueOf(channels.getChannelId()));
//            channel.setMore1(channels.getVfile());
//            channel.setImgAdrs(channels.getImage());
//
//            this.channels.add(channel);
//        }

        //load recent list
        activity.onGetChannels(new ArrayList<>());
    }

    public void filterChannels(String s) {
        Log.e(TAG, "filterChannels: " );
        trackEvent("search channel", "query", s);
        List<Channel> filtered = new ArrayList<>();
        for (Channel channel : channels)
            if (channel.getTitle().toLowerCase().contains(s.toLowerCase()))
                filtered.add(channel);

        if (filtered.size() > 0)
            activity.onFilterChannels(filtered);
        else
            activity.onSearchFailed(s);
    }

    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }
}

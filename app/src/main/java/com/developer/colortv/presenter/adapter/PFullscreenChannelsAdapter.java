package com.developer.colortv.presenter.adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.utils.CircleTransform;
import com.developer.colortv.R;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.presenter.activity.PFullscreenVideoActivity;
import com.developer.colortv.view.adapter.FullscreenChannelsAdapter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PFullscreenChannelsAdapter {
    List<Channel> channels;
    Context context;
    PFullscreenVideoActivity activity;
    private String TAG = getClass().getSimpleName();

    public PFullscreenChannelsAdapter(List<Channel> channels, Context context, PFullscreenVideoActivity activity) {
        this.channels = channels;
        this.context = context;
        this.activity = activity;
    }


    public void onBind(FullscreenChannelsAdapter.viewHolder holder, int position) {
        try {
            Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + channels.get(position).getImgAdrs());
            Picasso.get().load(uri).transform(new CircleTransform()).placeholder(R.drawable.satellite).into(holder.img);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            holder.txt.setText(channels.get(position).getTitle());
        } catch (Exception e){
            e.printStackTrace();
        }

        holder.llFavItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(channels.get(position));
            }
        });
    }

    private void onItemClick(Channel channel) {
        Log.e(TAG, "onItemClick: " + channel.getTitle() );
        trackEvent("FullscreenChannelClick", "channel name", channel.getTitle() + " - " + channel.getContent());
        activity.changeChannel(channel, 1);
    }

    public int getItemsCount() {
        return channels.size();
    }


    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }
}

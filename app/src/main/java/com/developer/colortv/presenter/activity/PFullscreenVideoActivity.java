package com.developer.colortv.presenter.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.App;
import com.developer.colortv.R;
import com.developer.colortv.model.dao.DAO;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.adapter.FullscreenChannelsAdapter;
import com.developer.colortv.presenter.adapter.PFullscreenChannelsAdapter;
import com.developer.colortv.ui.video.LandLayoutVideo;
import com.developer.colortv.view.activity.FullscreenVideoActivity;
import com.developer.colortv.view.interfaces.IFullscreenVideoActivity;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;
import com.shuyu.gsyvideoplayer.listener.GSYVideoProgressListener;
import com.shuyu.gsyvideoplayer.listener.VideoAllCallBack;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.shuyu.gsyvideoplayer.video.base.GSYVideoView.CURRENT_STATE_AUTO_COMPLETE;
import static com.shuyu.gsyvideoplayer.video.base.GSYVideoView.CURRENT_STATE_ERROR;
import static com.shuyu.gsyvideoplayer.video.base.GSYVideoView.CURRENT_STATE_NORMAL;

public class PFullscreenVideoActivity implements View.OnClickListener {

    private final String TAG = PFullscreenVideoActivity.class.getSimpleName();
    private IFullscreenVideoActivity activity;
    private Channel channel = new Channel();
    private LandLayoutVideo videoViewLand;
    private boolean isPlaying = true;
    private DAO dao;
    private List<Channel> channels;
    private int serverNum = 1;
    private VideoAllCallBack videoAllCallBack;
    private Timer mDismissControlViewTimer;
    private DismissControlViewTimerTask mDismissControlViewTimerTask;
    private int mCurrentState = -1;
    private boolean isLocked = false;
    private int selectedIndex = 0;
    private boolean first = true;
    FullscreenVideoActivity videoActivity;
    Context context;
    BroadcastReceiver channelReceiver;
//    private List<Channels> channelss;
//    private List<EPG> epgs;

    public PFullscreenVideoActivity(FullscreenVideoActivity activity) {
        this.activity = activity;
        channel = ((Channel) ((FullscreenVideoActivity) this.activity).getIntent().getParcelableExtra("channel"));
        dao = new DAO(activity.getApplicationContext());
        this.videoActivity = activity;
        this.context = activity.getApplicationContext();
        registerChannelReceiver();
        Log.e(TAG, "PFullscreenVideoActivity: " + channel.getTitle());

    }

    private void checkWhichFavIcon(LandLayoutVideo videoViewLand) {
        if (channel.getFav() == 1) {
            videoViewLand.getFavButton().setImageDrawable(context.getResources().getDrawable(R.drawable.favorite));
        } else {
            videoViewLand.getFavButton().setImageDrawable(context.getResources().getDrawable(R.drawable.heart));
        }
    }

    private void checkToShowServer() {
        if (dao.findItemCount(channel.getTitle()) > 1 ||
                (channel.getMore1() != null && channel.getMore2() != null
                        && !channel.getMore1().equalsIgnoreCase("") && !channel.getMore2().equalsIgnoreCase(""))) {
            videoViewLand.getBtnServers().setVisibility(VISIBLE);
        } else {
            videoViewLand.getBtnServers().setVisibility(GONE);
        }
    }

    private void registerChannelReceiver() {
        channelReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                channel = intent.getParcelableExtra("channel");
                loadVideo(videoViewLand, intent.getIntExtra("serverNum", 1));
            }
        };
        context.registerReceiver(channelReceiver, new IntentFilter("changeServer"));
    }

    public void loadVideo(LandLayoutVideo videoViewLand, int serverNum) {
        Log.e(TAG, "loadVideo: checkItem:" + channel.getTitle());
        channels = new ArrayList<>();

        checkWhichFavIcon(videoViewLand);
//        if (first)
//            loadEPG();
        channels = dao.getChannelsBySubject(channel.getSubject().split(",")[0]);
        videoAllCallBack = videoViewLand.getVideoAllCallBack();
        if (mDismissControlViewTimer != null)
            mDismissControlViewTimer.cancel();
        mDismissControlViewTimer = new Timer();
        mDismissControlViewTimer.schedule(new DismissControlViewTimerTask(), 4000);


        videoViewLand.getTxtChannelName().setText("شبکه " + channel.getTitle());

        this.videoViewLand = videoViewLand;
        Log.e(TAG, "loadVideo: " + channel.getMore1());
        this.serverNum = serverNum;
        switch (serverNum) {
            case 1:
                this.serverNum = 1;
                videoViewLand.setUp(channel.getMore1(), true, channel.getTitle());
                break;

            case 2:
                this.serverNum = 2;
                videoViewLand.setUp(channel.getMore2(), true, channel.getTitle());
                break;
        }
        videoViewLand.setAutoFullWithSize(true);

        checkToShowServer();

        setOnControllersClickListners();

        videoViewLand.getStartButton().setImageDrawable(App.context.getResources().getDrawable(R.drawable.ic_pause));

        videoViewLand.startPlayLogic();

        for (int i = 0; i < channels.size(); i++)
            if (channel == channels.get(i))
                selectedIndex = i;
    }

    private void setOnControllersClickListners() {
        videoViewLand.getStartButton().setOnClickListener(this::onClick);
        videoViewLand.getFavButton().setOnClickListener(this::onClick);
        videoViewLand.getSurfaceContainer().setOnClickListener(this::onClick);
        videoViewLand.getController().setOnClickListener(this::onClick);
        videoViewLand.getBtnServers().setOnClickListener(this::onClick);
//        videoViewLand.getMoreButton().setOnClickListener(this::onClick);
    }

    public void pauseAndResume() {
        if (isPlaying) {
            videoViewLand.onVideoPause();
            isPlaying = false;
            videoViewLand.getStartButton().setImageDrawable(App.context.getResources().getDrawable(R.drawable.ic_play));
            Log.e(TAG, "onClick: state playing");
        } else {
            videoViewLand.onVideoResume();
            isPlaying = true;
            videoViewLand.getStartButton().setImageDrawable(App.context.getResources().getDrawable(R.drawable.ic_pause));
            Log.e(TAG, "onClick: state pause");
            activity.onResumeVideo();
        }
        Log.e(TAG, "onClick");
    }

    private void gotoNextChannel() {
        try {
            int index = 0;
            for (int i = 0; i < channels.size(); i++) {
                if (channels.get(i).getId() == channel.getId())
                    index = i;
            }
            channel = channels.get(index + 1);
            Log.e(TAG, "gotoNextChannel: " + channel.getTitle());
            loadVideo(videoViewLand, 1);
        } catch (Exception e) {
            Log.e(TAG, "gotoNextChannel: exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void gotoPreChannel() {
        try {
            int index = 0;
            for (int i = 0; i < channels.size(); i++) {
                if (channels.get(i).getId() == channel.getId())
                    index = i;
            }
            channel = channels.get(index - 1);
            Log.e(TAG, "gotoNextChannel: " + channel.getTitle());
            loadVideo(videoViewLand, 1);
        } catch (Exception e) {
            Log.e(TAG, "gotoNextChannel: exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void addOrRemoveFav() {
        channel = dao.addToFav(channel);
        if (channel.getFav() == 1) {
            trackEvent("addToFav", "channel name", channel.getTitle());
//            Toast.makeText(App.context, "1", Toast.LENGTH_SHORT).show();
            videoViewLand.getFavButton().setImageDrawable(App.context.getResources().getDrawable(R.drawable.favorite));
        } else {
            trackEvent("removeFromFav", "channel name", channel.getTitle());
//            Toast.makeText(App.context, "0", Toast.LENGTH_SHORT).show();
            videoViewLand.getFavButton().setImageDrawable(App.context.getResources().getDrawable(R.drawable.heart));
        }
    }

//    private void reloadVideo() {
//        loadVideo(videoViewLand, serverNum);
//        videoViewLand.getReloadButton().setVisibility(GONE);
//    }

    private void hideController() {
        if (!isLocked) {
            videoAllCallBack.onClickBlankFullscreen(channel.getMore1(), "", this);

            cancelDismissControlViewTimer();
            mDismissControlViewTimer = new Timer();
            mDismissControlViewTimerTask = new DismissControlViewTimerTask();
            mDismissControlViewTimer.schedule(mDismissControlViewTimerTask, 4000);
            setViewShowState(videoViewLand.getController(), GONE);

            //hide fragment
            videoActivity.getSupportFragmentManager().popBackStack();
        }
    }

    private void showController() {
        if (!isLocked) {
            videoAllCallBack.onClickBlankFullscreen(channel.getMore1(), "", this);

            startDismissControlViewTimer();
            mDismissControlViewTimer = new Timer();
            mDismissControlViewTimerTask = new DismissControlViewTimerTask();
            mDismissControlViewTimer.schedule(mDismissControlViewTimerTask, 4000);
            setViewShowState(videoViewLand.getController(), VISIBLE);

            //hide fragment
            videoActivity.getSupportFragmentManager().popBackStack();
        }
    }

    private void startDismissControlViewTimer() {
        cancelDismissControlViewTimer();
        mDismissControlViewTimer = new Timer();
        mDismissControlViewTimerTask = new DismissControlViewTimerTask();
        mDismissControlViewTimer.schedule(mDismissControlViewTimerTask, 4000);
    }

    private void cancelDismissControlViewTimer() {
        if (mDismissControlViewTimer != null) {
            mDismissControlViewTimer.cancel();
            mDismissControlViewTimer = null;
        }
        if (mDismissControlViewTimerTask != null) {
            mDismissControlViewTimerTask.cancel();
            mDismissControlViewTimerTask = null;
        }

    }

//    public void unLockPlayer() {
//        isLocked = false;
//        activity.onUnlockPlayer();
//    }

    public void showBtnChannels() {
        setViewShowState(videoViewLand.getController(), GONE);
    }

    public void changeChannel(Channel channel, int serverNum) {
        first = false;
        this.channel = channel;
        loadVideo(videoViewLand, serverNum);
    }

//    public void showChannelEPG() {
//        //show epg in listview
//        try {
//            for (int i = 0; i < epgs.size(); i++)
//                if (channelss.get(i).getChannelName().equalsIgnoreCase(channel.getTitle())) {
//                    PEpgAdapter presenterAdapter = new PEpgAdapter(channelss.get(i).getEPG(), App.context, channelss.get(i).getCurrentProgram());
//                    activity.onShowEpg(presenterAdapter);
//
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    private class DismissControlViewTimerTask extends TimerTask {

        @Override
        public void run() {
            if (mCurrentState != CURRENT_STATE_NORMAL
                    && mCurrentState != CURRENT_STATE_ERROR
                    && mCurrentState != CURRENT_STATE_AUTO_COMPLETE) {
                new Handler(Looper.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {
                                setViewShowState(videoViewLand.getController(), GONE);
//                                if (mHideKey && mIfCurrentIsFullscreen && mShowVKey) {
//                                    hideNavKey(mContext);
//                                }
                            }
                        }
                );

            }
        }
    }

    private void setViewShowState(View view, int state) {
        view.setVisibility(state);
    }

//    private void lockPlayer() {
//        hideController();
//        isLocked = true;
//        activity.onLockPlayer();
//    }

    public void loadChannelsList() {
        PFullscreenChannelsAdapter presenterAdapter = new PFullscreenChannelsAdapter(channels, App.context, this);
//        activity.onShowChannelsFragment(presenterAdapter, selectedIndex);
        RecyclerView recyclerView = videoViewLand.getRclChannels();
        recyclerView.setLayoutManager(new LinearLayoutManager(App.context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(new FullscreenChannelsAdapter(presenterAdapter));
    }

//    private void loadEPG() {
//        EPGRetrofitBuilder builder = new EPGRetrofitBuilder();
//        builder.getDao().loadEPG().enqueue(new Callback<EPGResponse>() {
//            @Override
//            public void onResponse(Call<EPGResponse> call, Response<EPGResponse> response) {
//                channelss = new ArrayList<>();
//                for (int i = 0 ; i < response.body().getCategories().size() ; i++)
//                        channelss.addAll(response.body().getCategories().get(i).getChannels());
//
//                epgs = new ArrayList<>();
//                for (Channels channelItem : channelss)
//                    if (channelItem.getChannelName().trim().equalsIgnoreCase(channel.getTitle()))
//                        epgs = channelItem.getEPG();
//                Log.e(TAG, "onResponse: " );
//            }
//
//            @Override
//            public void onFailure(Call<EPGResponse> call, Throwable t) {
//                Log.e(TAG, "onFailure load epg: " + t.getMessage());
//            }
//        });
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlayTv:
                trackEvent("btnPlay", null, null);
                pauseAndResume();
                break;

            case R.id.btnFavLandscape:
                addOrRemoveFav();
                break;

            case R.id.surface_containerr:
                showController();
                break;

            case R.id.rlController:
                hideController();
                break;

            case R.id.btnServersLandscape:
                trackEvent("btnServer", null, null);
                activity.onShowServers(channel.getTitle());
                break;

        }
    }

    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }

}

package com.developer.colortv.presenter.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.utils.CircleTransform;
import com.developer.colortv.R;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.activity.FullscreenVideoActivity;
import com.developer.colortv.view.adapter.FavChannelAdapter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;
import com.squareup.picasso.Picasso;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PFavChannelsAdapter {
    List<Channel> favChannels;
    Context context;
    private String TAG = getClass().getSimpleName();

    public PFavChannelsAdapter(List<Channel> favChannels, Context context) {
        this.favChannels = favChannels;
        this.context = context;
    }


    public void onBind(FavChannelAdapter.viewHolder holder, int position) {
        try {
            Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/drawable/" + favChannels.get(position).getImgAdrs());
            Picasso.get().load(uri).transform(new CircleTransform()).placeholder(R.drawable.satellite).into(holder.imgFavItem);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            holder.txtFavItem.setText(favChannels.get(position).getTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.llFavItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(favChannels.get(position));
            }
        });
    }

    public int getItemsCount() {
        return favChannels.size();
    }


    private void onItemClick(Channel channel) {
//        Toast.makeText(context, channel.getTitle(), Toast.LENGTH_SHORT).show();
        trackEvent("FavoriteChannelClick", "channel name", channel.getTitle() + " - " + channel.getContent());

        if (channel.getContent().toLowerCase().contains("فیلترشکن)")) {
            if (checkVpn()) {
                Intent intent = new Intent(context, FullscreenVideoActivity.class);
                intent.putExtra("channel", channel);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else {
                context.sendBroadcast(new Intent().setAction("showVpnDialog"));
            }
        } else {
            if (channel.getContent().toLowerCase().contains("رادیو")) {
                context.sendBroadcast(new Intent().setAction("radioChannelChange").putExtra("channel", channel));
            } else {
                Intent intent = new Intent(context, FullscreenVideoActivity.class);
                intent.putExtra("channel", channel);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        }
    }

    private boolean checkVpn() {
        List<String> networkList = new ArrayList<>();
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.isUp())
                    networkList.add(networkInterface.getName());
            }
        } catch (Exception ex) {
            Log.e(TAG, "checkVpn: " + ex.getMessage());
        }

        return networkList.contains("tun0");
    }


    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));

        //firebase
    }
}

package com.developer.colortv.model.data_model;

import android.os.Parcel;
import android.os.Parcelable;

public class Channel implements Parcelable {
    String title,
            content,
            more1,
            more2,
            subject,
            imgAdrs;
    int id,
            fav,
            count;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMore1() {
        return more1;
    }

    public void setMore1(String more1) {
        this.more1 = more1;
    }

    public String getMore2() {
        return more2;
    }

    public void setMore2(String more2) {
        this.more2 = more2;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getFav() {
        return fav;
    }

    public void setFav(int fav) {
        this.fav = fav;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getImgAdrs() {
        return imgAdrs;
    }

    public void setImgAdrs(String imgAdrs) {
        this.imgAdrs = imgAdrs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Channel() {
    }

    public Channel(Parcel parcel) {
        readFromParcel(parcel);
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public Object createFromParcel(Parcel source) {
            return new Channel(source);
        }

        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

    private void readFromParcel(Parcel in) {

        title = in.readString();
        content = in.readString();
        more1 = in.readString();
        more2 = in.readString();
        count = in.readInt();
        fav = in.readInt();
        subject = in.readString();
        imgAdrs = in.readString();
        id = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(more1);
        dest.writeString(more2);
        dest.writeInt(count);
        dest.writeInt(fav);
        dest.writeString(subject);
        dest.writeString(imgAdrs);
        dest.writeInt(id);

    }
}

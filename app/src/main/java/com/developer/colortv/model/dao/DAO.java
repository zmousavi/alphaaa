package com.developer.colortv.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


import com.developer.colortv.model.data_model.Channel;

import java.util.ArrayList;
import java.util.List;

public class DAO {
    private DatabaseAccess databaseAccess;

    public DAO(Context context) {
        this.databaseAccess = DatabaseAccess.getInstance(context);
    }

    public List<Channel> getTvGlobal() {
        List<Channel> channels = new ArrayList<>();
        Cursor cursor = runQuery("SELECT * FROM 'tbl_tv' WHERE category LIKE 'daroni%' GROUP BY channel_name ORDER BY id");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }

    public List<Channel> getTvProvincialChannels() {
        List<Channel> channels = new ArrayList<>();
        Cursor cursor = runQuery("SELECT * FROM 'tbl_tv' WHERE category LIKE 'liveostani' GROUP BY channel_name");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }

    public List<Channel> getRadioChannels(String cat) {
        List<Channel> channels = new ArrayList<>();
        String catt = cat.replace("radio" ,"");
        Cursor cursor = runQuery("SELECT * FROM 'tbl_radio' WHERE category LIKE '" + cat + "'");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }

    private Cursor runQuery(String query) {
        System.out.println("QUERY: " + query);
        Cursor tmpCursor = databaseAccess.getDatabase().rawQuery(query, null);
        return tmpCursor;
    }

    public List<Channel> findChannelsByName(String channelName) {
        List<Channel> channels = new ArrayList<>();

//        Cursor cursor = runQuery("SELECT * FROM tbl_tv WHERE channel_name LIKE '" + channelName + "' AND category LIKE 'daroni%'");
        Cursor cursor = runQuery("SELECT * FROM tbl_tv WHERE channel_name LIKE '" + channelName +"'" );
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }

    public Channel findChannelByName(String lastChannel) {
        Channel channel = new Channel();
        Cursor cursor = runQuery("SELECT * FROM 'tv' WHERE title LIKE '%" + lastChannel + "'");
        cursor.moveToFirst();

        channel.setTitle(cursor.getString(0));
        channel.setContent(cursor.getString(1));
        channel.setMore1(cursor.getString(2));
        channel.setMore2(cursor.getString(3));
        channel.setCount(cursor.getInt(4));
        channel.setFav(cursor.getInt(5));
        channel.setSubject(cursor.getString(6));
        channel.setImgAdrs(cursor.getString(7));
        channel.setId(cursor.getInt(8));

        cursor.close();

        return channel;
    }

    public List<Channel> findChannelsByCat(String cat) {
        List<Channel> channels = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM tbl_tv WHERE channel_type LIKE '%" + cat + "%' AND category NOT LIKE '%daroni%' ORDER BY id");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }

    public List<Channel> getTvInternational() {
        List<Channel> channels = new ArrayList<>();
        Cursor cursor = runQuery("SELECT * FROM 'tv' WHERE content LIKE 'برون مرزی'");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }

    public List<Channel> getAllChannels() {
        List<Channel> channels = new ArrayList<>();
        Cursor cursor = runQuery("SELECT * FROM 'tbl_tv' GROUP BY channel_name");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }

        //add radios
        cursor = runQuery("SELECT * FROM 'tbl_radio'");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }

        cursor.close();

        return channels;
    }

    public List<Channel> findPlacesByCat(String cat) {
        List<Channel> channels = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM places WHERE content LIKE '%" + cat + "%'");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }

    public Channel findRadioChannelById(int pos) {
        Channel channel = new Channel();
        Cursor cursor = runQuery("SELECT * FROM 'radio' WHERE id LIKE '" + pos + "'");
        cursor.moveToFirst();

        channel.setTitle(cursor.getString(0));
        channel.setContent(cursor.getString(1));
        channel.setMore1(cursor.getString(2));
        channel.setMore2(cursor.getString(3));
        channel.setCount(cursor.getInt(4));
        channel.setFav(cursor.getInt(5));
        channel.setSubject(cursor.getString(6));
        channel.setImgAdrs(cursor.getString(7));
        channel.setId(cursor.getInt(8));

        cursor.close();

        return channel;
    }

    public Channel findTvChannelById(int pos) {
        Channel channel = new Channel();
        Cursor cursor = runQuery("SELECT * FROM 'tv' WHERE id LIKE '" + pos + "'");
        cursor.moveToFirst();

        channel.setTitle(cursor.getString(0));
        channel.setContent(cursor.getString(1));
        channel.setMore1(cursor.getString(2));
        channel.setMore2(cursor.getString(3));
        channel.setCount(cursor.getInt(4));
        channel.setFav(cursor.getInt(5));
        channel.setSubject(cursor.getString(6));
        channel.setImgAdrs(cursor.getString(7));
        channel.setId(cursor.getInt(8));

        cursor.close();

        return channel;
    }

    public Channel findRadioByName(String lastRadioChannelName) {
        Channel channel = new Channel();
        Cursor cursor = runQuery("SELECT * FROM 'radio' WHERE title LIKE '" + lastRadioChannelName + "'");
        cursor.moveToFirst();

        channel.setTitle(cursor.getString(0));
        channel.setContent(cursor.getString(1));
        channel.setMore1(cursor.getString(2));
        channel.setMore2(cursor.getString(3));
        channel.setCount(cursor.getInt(4));
        channel.setFav(cursor.getInt(5));
        channel.setSubject(cursor.getString(6));
        channel.setImgAdrs(cursor.getString(7));
        channel.setId(cursor.getInt(8));

        cursor.close();

        return channel;
    }

    public Channel findPlaceByName(String lastPlacesChannel) {
        Channel channel = new Channel();
        Cursor cursor = runQuery("SELECT * FROM 'places' WHERE title LIKE '" + lastPlacesChannel + "'");
        cursor.moveToFirst();

        channel.setTitle(cursor.getString(0));
        channel.setContent(cursor.getString(1));
        channel.setMore1(cursor.getString(2));
        channel.setMore2(cursor.getString(3));
        channel.setCount(cursor.getInt(4));
        channel.setFav(cursor.getInt(5));
        channel.setSubject(cursor.getString(6));
        channel.setImgAdrs(cursor.getString(7));
        channel.setId(cursor.getInt(8));

        cursor.close();

        return channel;
    }

    public int findItemCount(String title) {
        int count = 0;

        Cursor cursor = runQuery("SELECT COUNT(*) FROM 'tbl_tv' WHERE channel_name LIKE '" + title + "%' AND category LIKE 'daroni%'");
        cursor.moveToFirst();

        count = cursor.getInt(0);

        cursor.close();

        return count;
    }

    public void addToRecents(Channel channel) {
        ContentValues values = new ContentValues();
        values.put("title", channel.getTitle());
        values.put("content", channel.getContent());
        values.put("more1", channel.getMore1());
        values.put("more2", channel.getMore2());
        values.put("count", channel.getCount());
        values.put("subject", channel.getSubject());
        values.put("fav", channel.getFav());
        values.put("id", channel.getId());
        values.put("img_adrs", channel.getImgAdrs());

        databaseAccess.getDatabase().insert("recent", null, values);
    }

//    public List<Channel> getRecents() {
//        List<Channel> recents = new ArrayList<>();
//        Cursor cursor = runQuery("SELECT * FROM recent ORDER BY id DESC LIMIT 5");
//
//        cursor.moveToFirst();
//        while (!cursor.isAfterLast()) {
//            Channel channel = new Channel();
//            channel.setTitle(cursor.getString(0));
//            channel.setContent(cursor.getString(1));
//            channel.setMore1(cursor.getString(2));
//            channel.setMore2(cursor.getString(3));
//            channel.setCount(cursor.getInt(4));
//            channel.setFav(cursor.getInt(5));
//            channel.setSubject(cursor.getString(6));
//            channel.setImgAdrs(cursor.getString(7));
//            channel.setId(cursor.getInt(8));
//
//            recents.add(channel);
//            cursor.moveToNext();
//        }
//        cursor.close();
//
//        return recents;
//    }

    public boolean removeFromRecents(Channel channel) {
        return databaseAccess.getDatabase().delete("recent","id=" + channel.getId(), null) > 0;
    }

    public List<Channel> getTvTopRatedChannel() {
        List<Channel> list = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM tv WHERE count > 1 AND subject LIKE 'daroni%' GROUP BY title ORDER BY count DESC LIMIT 4 ");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            list.add(channel);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public List<Channel> getSatTopRatedChannel() {
        List<Channel> list = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM tv WHERE count > 1 AND subject LIKE 'bironi%' GROUP BY title ORDER BY count DESC LIMIT 4 ");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            list.add(channel);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public List<Channel> getRadioTopRatedChannel() {
        List<Channel> list = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM radio WHERE count > 1 GROUP BY title ORDER BY count DESC LIMIT 4 ");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            list.add(channel);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public List<Channel> getChannelsBySubject(String subject) {
        List<Channel> list = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM tbl_tv WHERE category LIKE '" + subject + "%'GROUP BY channel_name ORDER BY id ASC");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            list.add(channel);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public Channel addToFav(Channel channel) {
        ContentValues values = new ContentValues();
        values.put("id", channel.getId());
        values.put("channel_name", channel.getTitle());
        values.put("channel_type", channel.getContent());
        values.put("quality_low", channel.getMore1());
        values.put("quality_high", channel.getMore2());
        values.put("count", channel.getCount());
        if (channel.getFav() == 0) {
            values.put("favorite", 1);
            channel.setFav(1);
        } else {
            values.put("favorite", 0);
            channel.setFav(0);
        }
        values.put("category", channel.getSubject());
        values.put("image", channel.getImgAdrs());

        databaseAccess.getDatabase().update("tbl_tv", values, "id = ?", new String[]{String.valueOf(channel.getId())});
        databaseAccess.getDatabase().toString();

        return channel;
    }

    public List<Channel> getFavChannels() {
        List<Channel> channels = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM tbl_tv WHERE favorite = 1 GROUP BY channel_name ORDER BY id ASC");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();
        return channels;
    }

    public List<Channel> getIranianTVChannels() {
        List<Channel> channels = new ArrayList<>();

        Cursor cursor = runQuery("SELECT * FROM tbl_tv WHERE category LIKE 'daroni%' OR category LIKE 'liveostani' GROUP BY channel_name ORDER BY id ASC");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Channel channel = new Channel();
            channel.setTitle(cursor.getString(0));
            channel.setContent(cursor.getString(1));
            channel.setMore1(cursor.getString(2));
            channel.setMore2(cursor.getString(3));
            channel.setCount(cursor.getInt(4));
            channel.setFav(cursor.getInt(5));
            channel.setSubject(cursor.getString(6));
            channel.setImgAdrs(cursor.getString(7));
            channel.setId(cursor.getInt(8));

            channels.add(channel);
            cursor.moveToNext();
        }
        cursor.close();

        return channels;
    }
}

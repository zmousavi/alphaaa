package com.developer.colortv.model.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.developer.colortv.App;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.File;

public class DatabaseOpenHelper extends SQLiteAssetHelper
{
    private static final String DATABASE_NAME = "final.db";
    private static final int DATABASE_VERSION = 16;

    public DatabaseOpenHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        if (DATABASE_VERSION == 16) {
//            clearApplicationData();
            onUpgrade(this.getReadableDatabase(),13 , 16);
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

//        if (db.getVersion() == 2) {
//            Log.v(TAG, "Within onUpgrade. Old is: " + oldVersion + " New is: " + newVersion);
//            db.execSQL("DROP TABLE tabirkhaab_pnx");
//            db.execSQL(CREATE_TABLE);
////            copyDataBase();
//        }
        db.execSQL("PRAGMA user_version = 16");
    }

    public void clearApplicationData() {
        File cacheDirectory = App.context.getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    public static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }
}
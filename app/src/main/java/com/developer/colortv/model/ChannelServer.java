package com.developer.colortv.model;

import androidx.annotation.NonNull;

public class ChannelServer {
    public String title;
    public String content;
    public String more1;
    public String more2;
    public String subject;
    public String imgAdrs;
    public int id;
    public int fav;
    public int type;
    public int indexM1;
    public int setIndexM2;

    public int getIndexM2() {
        return setIndexM2;
    }

    public void setIndexM2(int setIndexM2) {
        this.setIndexM2 = setIndexM2;
    }

    public int getIndexM1() {
        return indexM1;
    }

    public void setIndexM1(int indexM1) {
        this.indexM1 = indexM1;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMore1() {
        return more1;
    }

    public void setMore1(String more1) {
        this.more1 = more1;
    }

    public String getMore2() {
        return more2;
    }

    public void setMore2(String more2) {
        this.more2 = more2;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getImgAdrs() {
        return imgAdrs;
    }

    public void setImgAdrs(String imgAdrs) {
        this.imgAdrs = imgAdrs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFav() {
        return fav;
    }

    public void setFav(int fav) {
        this.fav = fav;
    }

    @NonNull
    @Override
    public String toString() {
        String s = "title : " + getTitle()
                + "\ncontent : " + getContent()
                + "\nmore1 : " + getMore1()
                + "\nmore2 : " + getMore2()
                + "\nsubject : " + getSubject()
                + "\nimg : " + getImgAdrs();
        return s;
    }
}

package com.developer.colortv.utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;


public class ExoPlayerVideoHandler {
    private static ExoPlayerVideoHandler instance;
    private String TAG = ExoPlayerVideoHandler.class.getSimpleName();
    ImageView imageView;

    public static ExoPlayerVideoHandler getInstance() {
        if (instance == null) {
            instance = new ExoPlayerVideoHandler();
        }
        return instance;
    }

    private SimpleExoPlayer player;
    private Uri playerUri;
    private boolean isPlayerPlaying = true;

    private ExoPlayerVideoHandler() {
    }

    public void prepareExoPlayerForUri(Context context, Uri uri,
                                       SimpleExoPlayerView exoPlayerView, ProgressBar progressBar) {

        Log.e(TAG + "lifeCycle", "prepareExoPlayerForUri: 1" );
        if (context != null && uri != null && exoPlayerView != null) {
            if (!uri.equals(playerUri) || player == null) {
                Log.e(TAG + "lifeCycle", "prepareExoPlayerForUri: 2" );
                loadVideo(context, exoPlayerView, uri, progressBar);
            }
            player.clearVideoSurface();
            player.setVideoSurfaceView(
                    (SurfaceView) exoPlayerView.getVideoSurfaceView());
//            player.seekTo(player.getCurrentPosition() + 1);
            exoPlayerView.setPlayer(player);
            player.setPlayWhenReady(true);
        }
    }


    public void loadVideo(Context context, SimpleExoPlayerView videoView, Uri parse, ProgressBar progressBar) {
        Log.e(TAG + "lifeCycle", "loadVideo: 1" );
        this.player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter())), new DefaultLoadControl());
        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    progressBar.setVisibility(View.VISIBLE);
                } else if (playbackState == ExoPlayer.STATE_READY) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

//        this.player.addAnalyticsListener(new AnalyticsListener() {
//            @Override
//            public void onLoadCompleted(EventTime eventTime, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
//                progressBar.setVisibility(View.GONE);
//                try {
//                    player.setPlayWhenReady(true);
//                } catch (Exception e){}
//            }
//
//        });
//        videoView = new SimpleExoPlayerView(context);
//        videoView = (SimpleExoPlayerView) findViewById(R.id.videoView);
        videoView.setUseController(true);
        videoView.requestFocus();
        videoView.setPlayer(this.player);
//        parse = Uri.parse("https://irib-live.arvancloud.com/tv1/tv1_360/index.m3u8");
        DataSource.Factory defaultDataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "exoplayer2example"), new DefaultBandwidthMeter());
        DefaultExtractorsFactory defaultExtractorsFactory = new DefaultExtractorsFactory();
        final LoopingMediaSource loopingMediaSource = new LoopingMediaSource(new HlsMediaSource(parse, defaultDataSourceFactory, 1, null, null));
        this.player.prepare(loopingMediaSource);
        this.player.addListener(new ExoPlayer.EventListener() {
            public void onLoadingChanged(boolean z) {
            }

            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            public void onPositionDiscontinuity(int i) {
            }

            public void onRepeatModeChanged(int i) {
            }

            public void onSeekProcessed() {
            }

            public void onShuffleModeEnabledChanged(boolean z) {
//                Log.e(TAG, "onShuffleModeEnabledChanged" );
            }

            public void onTimelineChanged(Timeline timeline, Object obj, int i) {
//                Log.e(TAG, "onTimelineChanged" );
            }

            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
//                Log.e(TAG, "onTracksChanged" );
            }

            public void onPlayerStateChanged(boolean z, int i) {
//                Log.e(TAG, "onPlayerStateChanged" );
            }

            public void onPlayerError(ExoPlaybackException exoPlaybackException) {
//                Log.e(TAG, "onPlayerError" );
            }

        });
        this.player.setPlayWhenReady(true);

    }

    public void releaseVideoPlayer() {
        if (player != null) {
            player.release();
        }
        player = null;
    }

    public void goToBackground() {
        if (player != null) {
            isPlayerPlaying = player.getPlayWhenReady();
            player.setPlayWhenReady(false);
        }
    }

    public void stopPlayer() {
        if (player != null)
            player.stop();
    }

    public void goToForeground() {
        if (player != null) {
            player.setPlayWhenReady(isPlayerPlaying);
        }
    }

    public void muteSound() {
        if (player != null)
            player.setVolume(0);
    }

    public float getVolume() {
        float volume = 0;
        if (player != null)
            volume = player.getVolume();

        return volume;
    }

    public void setVolume(float volume) {
        if (player != null)
            player.setVolume(volume);
    }

    public void pause() {

        try {
            player.setPlayWhenReady(false);
            Log.e(TAG, "pause: " + player.getPlayWhenReady() );
        } catch (Exception e) {
        }
    }

    public void resume() {
        try {
            player.setPlayWhenReady(true);
        } catch (Exception e) {
        }
    }
}

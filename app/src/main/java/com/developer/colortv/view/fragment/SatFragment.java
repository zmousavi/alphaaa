package com.developer.colortv.view.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.developer.colortv.R;
import com.developer.colortv.view.adapter.CategoryAdapter;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class SatFragment extends Fragment {

    View view;

    @BindView(R.id.rclSatCategory)
    RecyclerView rclSatCategory;

    String[] catNames = {"موزیک", "فیلم", "خبری", "ورزش", "سرگرمی", "International TV",
            "Turkish TV", "World Fashion", "afghan", "Arabic TV", "India TV", "Kurdish TV", "Spanish TV", "مذهبی"};

    int[] catImgs = {R.drawable.cat__music, R.drawable.cat__movie, R.drawable.cat__news, R.drawable.cat__sport,
            R.drawable.cat__entertaitment, R.drawable.cat__international,
            R.drawable.cat__turkish, R.drawable.cat__fashion2, R.drawable.cat__afghan,
            R.drawable.cat__arabic, R.drawable.cat__indian, R.drawable.cat__kurdish,
            R.drawable.cat__spain, R.drawable.cat__religion};

    public static SatFragment newInstance(String param1, String param2) {
        SatFragment fragment = new SatFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(getContext(), new Crashlytics());
        AppCenter.start(getActivity().getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sat, container, false);

        ButterKnife.bind(this, view);

        rclSatCategory.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rclSatCategory.setHasFixedSize(true);
        rclSatCategory.setItemViewCacheSize(10);
        rclSatCategory.setAdapter(new CategoryAdapter(getContext(), catNames, catImgs, new CategoryAdapter.setOnItemClickListener() {
            @Override
            public void onClick(String category) {
                getContext().sendBroadcast(new Intent().setAction("changeTvChannelCategory")
                        .putExtra("type", "sat")
                        .putExtra("category", category));
            }
        }));
        return view;
    }

}

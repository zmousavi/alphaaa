package com.developer.colortv.view.interfaces;

public interface IRadioPlayerFragment {
    void onPlay(String channelName, String channelUrl, String img);
}

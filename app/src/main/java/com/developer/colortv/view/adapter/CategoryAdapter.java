package com.developer.colortv.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.developer.colortv.R;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.viewHolder> {
    String[] catNames;
    int[] catImgs;
    Context context;
    setOnItemClickListener listener;

    public CategoryAdapter(Context context, String[] catNames, int[] catImgs, setOnItemClickListener listener) {
        this.catImgs = catImgs;
        this.catNames = catNames;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CategoryAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.viewHolder holder, int position) {
        Picasso.get().load(catImgs[position]).into(holder.imgCatItem);

        holder.imgCatItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onClick(catNames[position]);

            }
        });
    }

    @Override
    public int getItemCount() {
        return catImgs.length;
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgCatItem)
        ImageView imgCatItem;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public interface setOnItemClickListener {
        public void onClick(String category);
    }
}

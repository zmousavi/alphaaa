package com.developer.colortv.view.fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.developer.colortv.R;
import com.developer.colortv.presenter.adapter.PChannelsAdapter;
import com.developer.colortv.presenter.fragment.PChannelsFragment;
import com.developer.colortv.view.adapter.ChannelsAdapter;
import com.developer.colortv.view.interfaces.IChannelsFragment;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class ChannelsFragment extends Fragment implements IChannelsFragment {

    View view;
    PChannelsFragment presenter;

    @BindView(R.id.rclChannels)
    RecyclerView rclChannels;
    @BindView(R.id.txtchannelsCat)
    TextView txtchannelsCat;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(getContext(), new Crashlytics());
        AppCenter.start(getActivity().getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_channels, container, false);

        ButterKnife.bind(this, view);

        presenter = new PChannelsFragment(this);
        presenter.loadChannels();

        return view;
    }

    @Override
    public void onLoadChannels(PChannelsAdapter presenter) {
        rclChannels.setLayoutManager(new GridLayoutManager(getContext(), 4));
        rclChannels.setHasFixedSize(true);
        rclChannels.setItemViewCacheSize(10);
        rclChannels.setAdapter(new ChannelsAdapter(presenter));
    }

    @Override
    public void onShowCategory(String category) {
        String cat = "", type = "";
        switch (category) {
            case "موزیک":
                cat = "موسیقی";
                type = "ماهواره ای";
                break;

            case "فیلم":
                cat = "فیلم";
                type = "ماهواره ای";
                break;

            case "خبری":
                cat = "خبری";
                type = "ماهواره ای";
                break;

            case "ورزش":
                cat = "ورزشی";
                type = "ماهواره ای";
                break;

            case "سرگرمی":
                cat = "سرگرمی";
                type = "ماهواره ای";
                break;

            case "International TV":
                type = "ماهواره ای";
                cat = "اینترنشنال";
                break;

            case "Turkish TV":
                type = "ماهواره ای";
                cat = "ترکی";
                break;

            case "World Fashion":
                type = "ماهواره ای";
                cat = "";
                break;

            case "afghan":
                cat = "افغانی";
                type = "ماهواره ای";
                break;

            case "Arabic TV":
                cat = "عربی";
                type = "ماهواره ای";
                break;

            case "India TV":
                cat = "هندی";
                type = "ماهواره ای";
                break;

            case "Kurdish TV":
                cat = "کوردی";
                type = "ماهواره ای";
                break;

            case "Spanish TV":
                cat = "اسپانیایی";
                type = "ماهواره ای";
                break;

            case "مذهبی":
                cat = "مذهبی";
                type = "ماهواره ای";
                break;

            case "radiomusic":
                cat = "موزیک";
                type = "رادیویی";
                break;

            case "radiosport":
                cat = "ورزشی";
                type = "رادیویی";
                break;

            case "radionews":
                cat = "خبری";
                type = "رادیویی";
                break;

            case "radiofunny":
                cat = "سرگرمی";
                type = "رادیویی";
                break;

            case "radiolearn":
                cat = "آموزشی";
                type = "رادیویی";
                break;

            case "radiomazhabi":
                cat = "مذهبی";
                type = "رادیویی";
                break;

            case "radioostani":
                cat = "استانی";
                type = "رادیویی";
                break;

            case "":
                cat = "";
                type = "ایرانی";
                break;
        }

        String s = "";
        if (cat.equalsIgnoreCase(""))
            s = "لیست شبکه های " + type;
        else
            s = "لیست شبکه های " + type + " دسته بندی " + cat;

        txtchannelsCat.setText(s);
    }
}

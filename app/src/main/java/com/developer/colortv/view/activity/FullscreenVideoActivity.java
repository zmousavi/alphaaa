package com.developer.colortv.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.R;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.presenter.activity.PFullscreenVideoActivity;
import com.developer.colortv.presenter.adapter.PFullscreenChannelsAdapter;
import com.developer.colortv.ui.video.LandLayoutVideo;
import com.developer.colortv.view.fragment.FragmentServers;
import com.developer.colortv.view.interfaces.IFullscreenVideoActivity;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;
import com.microsoft.appcenter.crashes.Crashes;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import me.cheshmak.android.sdk.advertise.CheshmakInterstitialAd;
import me.jessyan.autosize.AutoSizeConfig;

public class FullscreenVideoActivity extends ConnectionActivity implements IFullscreenVideoActivity {

    private String TAG = getClass().getSimpleName();
    PFullscreenVideoActivity presenter;
    @BindView(R.id.playerViewLand)
    LandLayoutVideo playerViewLand;
    @BindView(R.id.frmAdContainer)
    FrameLayout frmAdContainer;
    @BindView(R.id.loading)
    LottieAnimationView loading;

    CheshmakInterstitialAd interstitialAd;
    BroadcastReceiver hideLoadingReceiver, showLoadingReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AutoSizeConfig.getInstance().stop(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_fullscreen_video);
        registerReceivers();

        Fabric.with(this, new Crashlytics());
        AppCenter.start(getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);
        ButterKnife.bind(this);

        interstitialAd = new CheshmakInterstitialAd(this);
//        App.getInstance().loadAd(frmAdContainer);

        playerViewLand.setVideoAllCallBack(new GSYSampleCallBack() {
            @Override
            public void onTouchScreenSeekVolume(String url, Object... objects) {
                super.onTouchScreenSeekVolume(url, objects);
            }

            @Override
            public void onTouchScreenSeekLight(String url, Object... objects) {
                super.onTouchScreenSeekLight(url, objects);
            }

            @Override
            public void onClickBlank(String url, Object... objects) {
                super.onClickBlank(url, objects);
                Log.e(TAG, "onClickBlank: ");
            }

            @Override
            public void onClickBlankFullscreen(String url, Object... objects) {
                super.onClickBlankFullscreen(url, objects);
                Log.e(TAG, "onClickBlankFullscreen: ");
            }
        });
        playerViewLand.setAutoFullWithSize(true);

        presenter = new PFullscreenVideoActivity(this);
        presenter.loadVideo(playerViewLand, 1);
        presenter.loadChannelsList();
        Log.e(TAG, "onCreate: " + ((Channel) getIntent().getParcelableExtra("channel")).getTitle());
    }

    @Override
    public void onResumeVideo() {
        Log.e(TAG, "onResumeVideo: ");
        registerReceivers();
    }

    private void registerReceivers() {
        hideLoadingReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "onReceive: hideLoadingAnimation");
                loading.setVisibility(View.GONE);
            }
        };
        registerReceiver(hideLoadingReceiver, new IntentFilter("hideLoadingAnimation"));

        showLoadingReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "onReceive: showLoadingAnimation");
                loading.setVisibility(View.VISIBLE);
            }
        };
        registerReceiver(showLoadingReceiver, new IntentFilter("showLoadingAnimation"));
    }

    @Override
    public void onShowChannelsFragment(PFullscreenChannelsAdapter presenterAdapter, int selectedIndex) {
        Log.e(TAG, "onShowChannelsFragment: ");
    }

    @Override
    public void onShowServers(String channelName) {
        Log.e(TAG, "onShowServers: " + channelName);
        Fragment fragment = new FragmentServers();
        Bundle bundle = new Bundle();
        bundle.putString("channelName", channelName);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmServersContainer, fragment, "first");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right);
        fragmentTransaction.commit();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(hideLoadingReceiver);
        unregisterReceiver(showLoadingReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        playerViewLand.setVideoAllCallBack(null);
        GSYVideoManager.releaseAllVideos();

        Fragment fragment = new Fragment();
        try {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            if (fragmentList.size() > 0)
                fragment = fragmentList.get(fragmentList.size() - 1);
        } catch (Exception e) {
        }

        if (fragment instanceof FragmentServers) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        } else {
            try {
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                    Log.e(TAG, "onBackPressed: interstitial shown");
                }
//            App.getInstance().loadInterstitialAds();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }
}

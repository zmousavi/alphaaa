package com.developer.colortv.view.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.developer.colortv.R;
import com.developer.colortv.view.adapter.CategoryAdapter;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class RadioFragment extends Fragment {

    View view;

    @BindView(R.id.rclRadioCategory)
    RecyclerView rclRadioCategory;

    String[] catNames = {"radiomusic", "radiosport", "radionews", "radiofunny", "radiolearn", "radiomazhabi", "radioostani"};

    int[] catImgs = {R.drawable.cat__music, R.drawable.cat__sport, R.drawable.cat__news, R.drawable.cat__entertaitment,
            R.drawable.cat__learn, R.drawable.cat__religion, R.drawable.cat__provincial};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(getContext(), new Crashlytics());
        AppCenter.start(getActivity().getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_radio, container, false);

        ButterKnife.bind(this, view);

        rclRadioCategory.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rclRadioCategory.setHasFixedSize(true);
        rclRadioCategory.setItemViewCacheSize(10);
        rclRadioCategory.setAdapter(new CategoryAdapter(getContext(), catNames, catImgs, new CategoryAdapter.setOnItemClickListener() {
            @Override
            public void onClick(String category) {
                getContext().sendBroadcast(new Intent().setAction("changeTvChannelCategory")
                        .putExtra("type", "radio")
                        .putExtra("category", category));
            }
        }));

        return view;
    }

}

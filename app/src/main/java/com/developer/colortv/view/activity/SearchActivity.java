package com.developer.colortv.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.R;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.presenter.activity.PSearchActivity;
import com.developer.colortv.presenter.adapter.PSearchAdapter;
import com.developer.colortv.view.adapter.SearchAdapter;
import com.developer.colortv.view.fragment.RadioPlayerFragment1;
import com.developer.colortv.view.interfaces.ISearchActivity;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class SearchActivity extends AppCompatActivity implements ISearchActivity {
    public final static String PERSIAN_STRING = "ا آ ب پ ت ث ج چ ح خ د ذ ر ز ژ س ش ص ض ط ظ ع غ ف ق ک گ ل م ن و ه ی";
    public final static String LATIN_STRING = "a b c d e f g h i j k l m n o p q r s t u v w x y z";

    private String TAG = SearchActivity.class.getSimpleName();
    @BindView(R.id.btnClearSearchView)
    ImageView btnClearSearchBox;
    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.edtSearch)
    EditText autoCompleteTextView;
    @BindView(R.id.recyclerSearch)
    RecyclerView recyclerView;
    @BindView(R.id.txtSearchFailed)
    TextView txtSearchFailed;
    @BindView(R.id.llSearchFailed)
    LinearLayout llSearchFailed;
    @BindView(R.id.frmSearch)
    FrameLayout frmSearch;

    PSearchActivity presenter;
    List<Channel> channels = new ArrayList<>();
    PSearchAdapter presenterAdapter;
    SearchAdapter adapter;
    boolean firstTime = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Fabric.with(this, new Crashlytics());
        AppCenter.start(getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);
        ButterKnife.bind(this);

        presenter = new PSearchActivity(this);
        presenter.loadChannels();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnClearSearchBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstTime = true;
                autoCompleteTextView.setText("");
            }
        });


        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                firstTime = false;
                presenter.filterChannels(s.toString());

                if (s.toString().length() > 0) {
                    btnClearSearchBox.setVisibility(View.VISIBLE);

                    if (PERSIAN_STRING.contains(s.toString().toLowerCase().substring(0, 1))) {
                        autoCompleteTextView.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        autoCompleteTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSans.ttf"));
                    }

                    if (LATIN_STRING.contains(s.toString().toLowerCase().substring(0, 1))) {
                        autoCompleteTextView.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                        autoCompleteTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Product Sans Regular.ttf"));
                    }

                } else {
                    firstTime = true;
                    presenterAdapter = new PSearchAdapter(SearchActivity.this, new ArrayList<>());
                    recyclerView.setAdapter(new SearchAdapter(presenterAdapter, firstTime));
                    autoCompleteTextView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSans.ttf"));
                    autoCompleteTextView.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                    btnClearSearchBox.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Answers.getInstance().logCustom(new CustomEvent("after search").putCustomAttribute("value", autoCompleteTextView.getText().toString()));
            }
        });


    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        hideKeyboard(this);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        hideKeyboard(this);
        super.onPause();
    }

    @Override
    public void onFilterChannels(List<Channel> channels) {
        Log.e(TAG, "onFilterChannels : " + channels.size());
        recyclerView.setVisibility(View.VISIBLE);
        llSearchFailed.setVisibility(View.GONE);

        presenterAdapter = new PSearchAdapter(SearchActivity.this, channels);
        adapter = new SearchAdapter(presenterAdapter, firstTime);
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        recyclerView.setAdapter(adapter);

        this.channels = channels;
    }

    @Override
    public void onSearchFailed(String channelName) {
        recyclerView.setVisibility(View.GONE);
        llSearchFailed.setVisibility(View.VISIBLE);
//        String name = "<font color='red'>" + channelName + "</font><font color='red'>789</font>";
//        String namee = "هیچ کانالی مربوط به " + name + " پیدا نشد.";
//        txtSearchFailed.setText(Html.fromHtml(namee), TextView.BufferType.SPANNABLE);
    }

    @Override
    public void showRadioPlayer(Channel channel) {
        Fragment fragment = new RadioPlayerFragment1();
        Bundle bundle = new Bundle();
        bundle.putParcelable("channel", channel);
        fragment.setArguments(bundle);
        addFragment(fragment);
//        Intent intent = new Intent(SearchActivity.this, RadioPlayerFragment.class);
//        intent.putExtra("channel", channel);
//        startActivity(intent);
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmSearch, fragment, "first");
        fragmentTransaction.addToBackStack("first");
        fragmentTransaction.commit();
    }

    @Override
    public void onGetChannels(List<Channel> channels) {
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        presenterAdapter = new PSearchAdapter(SearchActivity.this, channels);
        adapter = new SearchAdapter(presenterAdapter, firstTime);
        recyclerView.setAdapter(adapter);
    }
}

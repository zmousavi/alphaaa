package com.developer.colortv.view.interfaces;

import com.developer.colortv.presenter.adapter.PFullscreenChannelsAdapter;

public interface IFullscreenVideoActivity {
    void onResumeVideo();

    void onShowChannelsFragment(PFullscreenChannelsAdapter presenterAdapter, int selectedIndex);

    void onShowServers(String channelName);
}

package com.developer.colortv.view.interfaces;

import com.developer.colortv.model.data_model.Channel;

import java.util.List;

public interface IFragmentServers {

    void onGetServers(List<Channel> channels);

}

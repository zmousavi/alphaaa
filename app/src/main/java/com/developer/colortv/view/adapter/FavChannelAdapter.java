package com.developer.colortv.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.developer.colortv.R;
import com.developer.colortv.presenter.adapter.PFavChannelsAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FavChannelAdapter extends RecyclerView.Adapter<FavChannelAdapter.viewHolder> {
    PFavChannelsAdapter presenter;

    public FavChannelAdapter(PFavChannelsAdapter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public FavChannelAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_fav_channel, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavChannelAdapter.viewHolder holder, int position) {
        presenter.onBind(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemsCount();
    }

    public class viewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.imgFavItem)
        public ImageView imgFavItem;
        @BindView(R.id.txtFavItem)
        public TextView txtFavItem;
        @BindView(R.id.llFavItem)
        public LinearLayout llFavItem;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}

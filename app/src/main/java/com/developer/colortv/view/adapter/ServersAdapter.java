package com.developer.colortv.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.developer.colortv.R;
import com.developer.colortv.model.ChannelServer;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.fragment.FragmentServers;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.analytics.EventProperties;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ServersAdapter extends RecyclerView.Adapter<ServersAdapter.viewHolder> {
    private ArrayList<ChannelServer> _list;
    private FragmentServers fragment;
    Context context;
    int selectedIndex;
    private String TAG = "ServersAdapter";
    Typeface fontMedium, fontRegular;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public ServersAdapter(Context context, ArrayList<ChannelServer> listt, FragmentServers fragmentServers) {
        this.fragment = fragmentServers;
        this.context = context;
        this._list = listt;

        fontMedium = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans_Medium.ttf");
        fontRegular = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSans.ttf");

        preferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE);
        editor = preferences.edit();

//        selectedIndex = listt.size() - 2;
        try {
            selectedIndex = preferences.getInt(listt.get(0).getTitle() + "ServerPosition", listt.size() - 2);
        } catch (Exception e) {
            selectedIndex = listt.size() - 2;
        }
    }

    @NonNull
    @Override
    public ServersAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_server, viewGroup, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int position) {
        viewHolder holder = (viewHolder) viewHolder;
        ChannelServer item = this._list.get(position);
        checkItem(item, holder);
//        Log.e(TAG, "onBindViewHolder: " + item.toString());

        holder.txtServerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(_list.get(position).getTitle() + "ServerPosition", position);
                editor.apply();
                Channel channel = new Channel();
                channel.setTitle(item.getTitle());
                channel.setContent(item.getContent());
                channel.setMore1(item.getMore1());
                channel.setMore2(item.getMore2());
                channel.setSubject(item.getSubject());
                channel.setFav(item.getFav());
                channel.setId(item.getId());
                channel.setImgAdrs(item.getImgAdrs());

                selectedIndex = position;

                if (item.getType() == 1) {
                    Log.e(TAG, "checkItem: index m1  " + item.getIndexM1());
                    // TODO: 1/23/2019 show more 1
                    context.sendBroadcast(new Intent().setAction("changeServer").putExtra("channel", channel)
                            .putExtra("serverNum", 1)
                            .putExtra("serverName", "سرور " + item.getIndexM1() + " - " + " کیفیت پایین"));
                } else {
                    // TODO: 1/23/2019 show more 2
                    Log.e(TAG, "checkItem: index m2 " + item.getIndexM2());
                    context.sendBroadcast(new Intent().setAction("changeServer").putExtra("channel", channel)
                            .putExtra("serverNum", 2)
                            .putExtra("serverName", "سرور " + item.getIndexM2() + " - " + " کیفیت بالا"));
                }

                notifyDataSetChanged();
            }
        });
        if (position == selectedIndex) {
            //set textView's color
            viewHolder.txtServerName.setTextColor(context.getResources().getColor(R.color.white));
            viewHolder.txtServerName.setTypeface(fontMedium);
            viewHolder.txtServerName.setTextSize(16);
            viewHolder.txtServerName.setBackground(context.getResources().getDrawable(R.drawable.bg_list_item_server_selected));
//        viewHolder.imgCheck.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtServerName.setTextColor(Color.parseColor("#bcb9b9"));
            viewHolder.txtServerName.setTypeface(fontRegular);
            viewHolder.txtServerName.setTextSize(14);
            viewHolder.txtServerName.setBackground(context.getResources().getDrawable(R.drawable.bg_list_item_server));
//        viewHolder.imgCheck.setVisibility(View.GONE);
        }


//
//        viewHolderSearch.itemServer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                fragment.hellowSadegh();
//            }
//        });

    }

    private void checkItem(ChannelServer item, viewHolder holder) {
//        Log.e(TAG, "checkItem: " + item.getType());
        if (item.getType() == 1) {
//            Log.e(TAG, "checkItem: index m1  " + item.getIndexM1());
            // TODO: 1/23/2019 show more 1
            holder.txtServerName.setText("سرور " + item.getIndexM1() + "-" + " کیفیت پایین");
        } else {
//            Log.e(TAG, "checkItem: index m2 " + item.getIndexM2());
            holder.txtServerName.setText("سرور " + item.getIndexM2() + "-" + " کیفیت بالا");
            // TODO: 1/23/2019 show more 2
        }
    }

    @Override
    public int getItemCount() {
//        int count = 0;
//        for (Channel channel : channelList) {
//            if (channel.getMore1() != null && !TextUtils.isEmpty(channel.getMore1()))
//                count++;
//            if (channel.getMore2() != null && !TextUtils.isEmpty(channel.getMore2()))
//                count++;
//        }
        return _list.size();
    }

    public void hellowZahra() {
        Log.e(TAG, "hellowZahra: ");
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtServerItem)
        public TextView txtServerName;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public interface setOnItemClickListener {
        public void onClick(ChannelServer channel);
    }


    public void trackEvent(String eventKey, String eventName, String eventValue) {
        //hockey
        Analytics.trackEvent(eventKey, new EventProperties().set(eventName, eventValue));

        //fabric
        Answers.getInstance().logCustom(new CustomEvent(eventKey)
                .putCustomAttribute(eventName, eventValue));
    }
}

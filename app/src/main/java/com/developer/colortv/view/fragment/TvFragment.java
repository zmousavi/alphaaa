package com.developer.colortv.view.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.developer.colortv.R;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class TvFragment extends Fragment {

    View view;
    private String TAG = getClass().getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(getContext(), new Crashlytics());
        AppCenter.start(getActivity().getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tv, container, false);

        ButterKnife.bind(this, view);

        return view;
    }


    @OnClick(R.id.btnGlobalTv)
    public void globalTvClick() {
        Log.e(TAG, "globalTvClick: ");
        getContext().sendBroadcast(new Intent().setAction("changeTvChannelCategory")
                .putExtra("type", "tv").putExtra("category", "global"));
    }

    @OnClick(R.id.btnProvincialTv)
    public void provincialTvClick() {
        Log.e(TAG, "provincialTvClick: ");
        getContext().sendBroadcast(new Intent().setAction("changeTvChannelCategory")
                .putExtra("type", "tv").putExtra("category", "provincial"));
    }

}

package com.developer.colortv.view.interfaces;


import com.developer.colortv.model.data_model.Channel;

import java.util.List;

public interface ISearchActivity {
    public void onGetChannels(List<Channel> channels);

    public void onFilterChannels(List<Channel> channels);

    public void onSearchFailed(String channelName);

    public void showRadioPlayer(Channel channel);
}

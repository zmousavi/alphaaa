package com.developer.colortv.view.interfaces;

import com.developer.colortv.presenter.adapter.PChannelsAdapter;

public interface IChannelsFragment {
    void onLoadChannels(PChannelsAdapter presenter);

    void onShowCategory(String category);
}

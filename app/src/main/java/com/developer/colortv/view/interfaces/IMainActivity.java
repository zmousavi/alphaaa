package com.developer.colortv.view.interfaces;

import com.developer.colortv.presenter.adapter.PFavChannelsAdapter;

public interface IMainActivity {

    void onGetFavChannels(PFavChannelsAdapter presenterAdapter);

}

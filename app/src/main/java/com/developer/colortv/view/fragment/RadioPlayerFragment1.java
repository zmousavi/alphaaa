package com.developer.colortv.view.fragment;


import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.crashlytics.android.Crashlytics;
import com.developer.colortv.utils.CircleTransform;
import com.developer.colortv.R;
import com.developer.colortv.presenter.fragment.PRadioPlayerFragment;
import com.developer.colortv.view.interfaces.IRadioPlayerFragment;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class RadioPlayerFragment1 extends Fragment implements IRadioPlayerFragment {

    View view;
    PRadioPlayerFragment presenter;
    MediaPlayer mediaPlayer;

    @BindView(R.id.txtRadio)
    TextView txtRadio;
    @BindView(R.id.imgRadio)
    ImageView imgRadio;
    @BindView(R.id.btnPlayPauseRadio)
    ImageView btnPlayPauseRadio;
    @BindView(R.id.llRadioPlayer)
    LinearLayout llRadioPlayer;
    @BindView(R.id.animEqualizer)
    LottieAnimationView animEqualizer;

    private String TAG = getClass().getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(getContext(), new Crashlytics());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_radio_player, container, false);

        ButterKnife.bind(this, view);

        presenter = new PRadioPlayerFragment(this);

        presenter.playRadio();
        presenter.loadChannelsByCat();

        llRadioPlayer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        return view;
    }

    @OnClick(R.id.btnPlayPauseRadio)
    public void onPlayPauseClick() {
        if (mediaPlayer.isPlaying()){
            mediaPlayer.pause();
            animEqualizer.pauseAnimation();
            btnPlayPauseRadio.setImageDrawable(getResources().getDrawable(R.drawable.ic_play));
        } else {
            mediaPlayer.start();
            animEqualizer.playAnimation();
            btnPlayPauseRadio.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
        }
    }

    @OnClick(R.id.btnNextRadio)
    public void onNextClick() {
        presenter.goToNextChannel();
    }

    @OnClick(R.id.btnPreRadio)
    public void onPreClick() {
        presenter.gotoPreChannel();
    }

    @Override
    public void onPlay(String channelName, String channelUrl, String img) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        mediaPlayer = new MediaPlayer();
        //set text
        txtRadio.setText(channelName);
        //set image
        Uri uri = Uri.parse("android.resource://" + getContext().getPackageName() + "/drawable/" + img);
        Picasso.get().load(uri).transform(new CircleTransform()).into(imgRadio);
        try {
            mediaPlayer.setDataSource(channelUrl);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    animEqualizer.playAnimation();
//                    animImage();
                    Log.e(TAG, "onPrepared: " );
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void animImage() {
        Animation controller = AnimationUtils.loadAnimation(getContext(), R.anim.scale);
        controller.setRepeatCount(Animation.INFINITE);
        controller.setRepeatMode(Animation.REVERSE);
        imgRadio.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.scale));
    }

    @OnClick(R.id.btnBackRadioPlayer)
    public void onBack() {
        if (mediaPlayer != null)
            mediaPlayer.stop();
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }
}

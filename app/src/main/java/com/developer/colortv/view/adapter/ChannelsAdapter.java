package com.developer.colortv.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.developer.colortv.R;
import com.developer.colortv.presenter.adapter.PChannelsAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelsAdapter extends RecyclerView.Adapter<ChannelsAdapter.viewHolder> {
    PChannelsAdapter presenter;

    public ChannelsAdapter(PChannelsAdapter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ChannelsAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_channel, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChannelsAdapter.viewHolder holder, int position) {
        presenter.onBind(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }

    public class viewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.imgChannelItem)
        public ImageView imgChannelItem;
        @BindView(R.id.txtChannelItem)
        public TextView txtChannelItem;
        @BindView(R.id.llChannelItem)
        public LinearLayout llChannelItem;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}

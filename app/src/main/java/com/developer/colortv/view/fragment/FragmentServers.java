package com.developer.colortv.view.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.developer.colortv.R;
import com.developer.colortv.model.ChannelServer;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.view.adapter.ServersAdapter;
import com.developer.colortv.presenter.fragment.PFragmentServers;
import com.developer.colortv.view.interfaces.IFragmentServers;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentServers extends Fragment implements IFragmentServers {

    @BindView(R.id.recyclerServers)
    RecyclerView recyclerView;

    PFragmentServers presenter;
    Bundle bundle;
    String channelName;
    View view;
    private String TAG = getClass().getSimpleName();
    private ServersAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(getContext(), new Crashlytics());
        AppCenter.start(getActivity().getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_servers, container, false);

        ButterKnife.bind(this, view);
        initViews();

        bundle = getArguments();
        channelName = bundle.getString("channelName");

        presenter = new PFragmentServers(this);
        presenter.loadServers(channelName);

        return view;
    }

    private void initViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onGetServers(List<Channel> channels) {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        ArrayList<ChannelServer> listt = getListProcces(channels);
        if (listt.size() > 0) {
            Log.e(TAG, "onGetServers: " + listt.size());
            adapter = new ServersAdapter(getContext(), listt, FragmentServers.this);

            recyclerView.setAdapter(adapter);

        } else {
            Log.e(TAG, "onGetServers: lis is null  ");
        }

    }

    // TODO: 1/23/2019 Written by Sadegh :)))
    private ArrayList<ChannelServer> getListProcces(List<Channel> channels) {
        ArrayList<ChannelServer> list = new ArrayList<>();
        int indexM1 = 1;
        int indexM2 = 1;

        for (Channel ch : channels) {
            if (ch.getMore1() != null && !TextUtils.isEmpty(ch.getMore1())) {
                ChannelServer sadegh = new ChannelServer();
                sadegh.setContent(ch.getContent());
                sadegh.setMore1(ch.getMore1());
                sadegh.setMore2(ch.getMore2());
                sadegh.setSubject(ch.getSubject());
                sadegh.setContent(ch.getContent());
                sadegh.setSubject(ch.getSubject());
//                sadegh.setCount(ch.getCount());
//                sadegh.setMore4(ch.getMore4());
//                sadegh.setMore5(ch.getMore5());
//                sadegh.setMore6(ch.getMore6());
                sadegh.setTitle(ch.getTitle());
                sadegh.setIndexM1(indexM1);
                indexM1 = indexM1 + 1;

                sadegh.setType(1);
                list.add(sadegh);
            } else {
                Log.e(TAG, "getListProcces: chanel get more 1 is null ");
            }
            if (ch.getMore2() != null && !TextUtils.isEmpty(ch.getMore2())) {
                ChannelServer sadegh = new ChannelServer();
                sadegh.setContent(ch.getContent());
                sadegh.setTitle(ch.getTitle());
                sadegh.setMore1(ch.getMore1());
                sadegh.setMore2(ch.getMore2());
                sadegh.setSubject(ch.getSubject());
                sadegh.setContent(ch.getContent());
                sadegh.setSubject(ch.getSubject());
                sadegh.setType(2);
                list.add(sadegh);
                sadegh.setIndexM2(indexM2);
                indexM2 = indexM2 + 1;
            } else {
                Log.e(TAG, "getListProcces: chanel get more 2 is null ");
            }
        }

        return list;

    }

    public void hellowSadegh() {
        Log.e(TAG, "hellowSadegh: hellow kaley");
    }

    public void chengeAdapter() {
        adapter.hellowZahra();
    }

}

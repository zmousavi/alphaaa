package com.developer.colortv.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.developer.colortv.App;
import com.developer.colortv.R;
import com.developer.colortv.location.LocationFinder;
import com.developer.colortv.location.LocationRequestCallback;
import com.developer.colortv.location.SmartModel;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.presenter.activity.PMainActivity;
import com.developer.colortv.presenter.adapter.PFavChannelsAdapter;
import com.developer.colortv.view.adapter.FavChannelAdapter;
import com.developer.colortv.view.fragment.ChannelsFragment;
import com.developer.colortv.view.fragment.RadioFragment;
import com.developer.colortv.view.fragment.RadioPlayerFragment1;
import com.developer.colortv.view.fragment.SatFragment;
import com.developer.colortv.view.fragment.TvFragment;
import com.developer.colortv.view.interfaces.IMainActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.internal.BaselineLayout;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ronash.pushe.Pushe;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends ConnectionActivity implements IMainActivity {

    @BindView(R.id.rclFavs)
    RecyclerView rclFavs;
    @BindView(R.id.bottomNavigation)
    BottomNavigationView bnv;
    @BindView(R.id.frmAd)
    FrameLayout frmAdContainer;
    @BindView(R.id.loading)
    ProgressBar loading;

    PMainActivity presenter;
    BroadcastReceiver categoryChangeReceiver, radioChannelChangeReceiver, vpnReceiver;
    private String TAG = getClass().getSimpleName();
    String countryName = "";
    boolean isIranian = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Pushe.initialize(this, true);
        Fabric.with(this, new Crashlytics());
        AppCenter.start(getApplication(), "2e5fe6cf-f560-405c-abc5-fbaacf613166"
                , Analytics.class, Crashes.class);

        ButterKnife.bind(this);

        loading.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.colorAccent), android.graphics.PorterDuff.Mode.SRC_IN);

        setNavigationTypeface();

        registerReceivers();
        try {
            checkIp();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showCheshmakBanner() {

    }

    @OnClick(R.id.btnSearch)
    public void onBtnSearchClick() {
        startActivity(new Intent(MainActivity.this, SearchActivity.class));
    }

    public void setNavigationTypeface() {
        final Typeface avenirHeavy = Typeface.createFromAsset(this.getAssets(), "fonts/IRANSans.ttf"); //replace it with your own font
        ViewGroup navigationGroup = (ViewGroup) bnv.getChildAt(0);
        for (int i = 0; i < navigationGroup.getChildCount(); i++) {
            ViewGroup navUnit = (ViewGroup) navigationGroup.getChildAt(i);
            for (int j = 0; j < navUnit.getChildCount(); j++) {
                View navUnitChild = navUnit.getChildAt(j);
                if (navUnitChild instanceof BaselineLayout) {
                    BaselineLayout baselineLayout = (BaselineLayout) navUnitChild;
                    for (int k = 0; k < baselineLayout.getChildCount(); k++) {
                        View baselineChild = baselineLayout.getChildAt(k);
                        if (baselineChild instanceof TextView) {
                            TextView textView = (TextView) baselineChild;
                            textView.setTextSize(11);
                            textView.setTypeface(avenirHeavy);
                        }
                    }
                }
            }
        }
    }

    private void showAdBanner() {
        App.getInstance().loadAd(frmAdContainer);
    }

    private boolean checkIp() {

        LocationFinder.builder(this, this, new LocationRequestCallback() {
            public void onLocationResult(SmartModel location) {
                if (location != null) {
                    try {
                        if (location.ipLocationV1.countryName.equalsIgnoreCase("Iran")
                                || checkVpn()) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            loading.setVisibility(View.GONE);
                                            showContent();

//                                            showIranianTV();
                                        }
                                    });
                                }
                            }).start();
                        } else {
                            showIranianTV();
//                            System.exit(0);
                        }

                        if (location.ipLocationV1.countryName.equalsIgnoreCase("Iran"))
                            countryName = "Iran";
                    } catch (Exception e) {

                    }
                }
            }

            public void onFailedRequest(String result) {
                Log.e(TAG, "onLocationResult: " + result);

            }
        }).ipv1().build().Start();

        if (countryName.equalsIgnoreCase("Iran"))
            return true;
        else
            return false;
    }

    private void showIranianTV() {
        isIranian = true;
        addIranianTVFragment();
    }

    private void showContent() {
        presenter = new PMainActivity(this);
        presenter.loadFavChannels();

//        showAdBanner();
        showCheshmakBanner();

        addFragment(new SatFragment());
        bnv.setSelectedItemId(R.id.bottom_sat);
        bnv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                menuItem.setChecked(true);
                switch (menuItem.getItemId()) {
                    case R.id.bottom_tv:
                        addFragment(new TvFragment());
                        break;

                    case R.id.bottom_sat:
                        addFragment(new SatFragment());
                        break;

                    case R.id.bottom_radio:
                        addFragment(new RadioFragment());
                        break;
                }
                return false;
            }
        });
    }

    private boolean checkVpn() {
        List<String> networkList = new ArrayList<>();
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.isUp())
                    networkList.add(networkInterface.getName());
            }
        } catch (Exception ex) {
            Log.e(TAG, "checkVpn: " + ex.getMessage());
        }

        return networkList.contains("tun0");
    }


    public void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmMain, fragment, "first");
        fragmentTransaction.addToBackStack("first");
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void addRadioFragment(Channel channel) {
        Fragment fragment = new RadioPlayerFragment1();
        Bundle bundle = new Bundle();
        bundle.putParcelable("channel", channel);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmMainActivity, fragment, "first");
        fragmentTransaction.addToBackStack("second");
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void addIranianTVFragment() {
        Fragment tvFragment = new ChannelsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("category", "");
        bundle.putString("type", "iranianTV");
        tvFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frmMainActivity, tvFragment, "first");
        fragmentTransaction.addToBackStack("first");
        fragmentTransaction.commitAllowingStateLoss();
    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        Log.e(TAG, "onRestart: ");
//        checkIp();
//    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: ");
        super.onResume();

    }

    private void registerReceivers() {
        categoryChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = new Bundle();

                switch (intent.getStringExtra("type")) {
                    case "tv":
                        switch (intent.getStringExtra("category")) {
                            case "global":
                                bundle.putString("type", "tv");
                                bundle.putString("category", "global");
                                break;

                            case "provincial":
                                bundle.putString("type", "tv");
                                bundle.putString("category", "provincial");
                                break;
                        }
                        break;

                    case "sat":
                        bundle.putString("type", "sat");
                        bundle.putString("category", intent.getStringExtra("category"));
                        break;

                    case "radio":
                        bundle.putString("type", "radio");
                        bundle.putString("category", intent.getStringExtra("category"));
                        break;
                }

                Fragment fragment = new ChannelsFragment();
                fragment.setArguments(bundle);
                addFragment(fragment);
            }
        };
        registerReceiver(categoryChangeReceiver, new IntentFilter("changeTvChannelCategory"));


        radioChannelChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                addRadioFragment((Channel) intent.getExtras().getParcelable("channel"));
            }
        };
        registerReceiver(radioChannelChangeReceiver, new IntentFilter("radioChannelChange"));

        registerReceiver(internetConnectionReciever, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        vpnReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                showVpnDialog();
                Log.e(TAG, "onReceive: vpnReceiver");
            }
        };
        registerReceiver(vpnReceiver, new IntentFilter("showVpnDialog"));
    }

    private void showVpnDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.CustomAlertDialog);
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_vpn, null, false);
        builder.setView(view);
        AlertDialog dialog = builder.create();

        AppCompatButton btnClose = view.findViewById(R.id.btnCloseVpnDialog);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    public BroadcastReceiver internetConnectionReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo activeWIFIInfo = connectivityManager.getNetworkInfo(connectivityManager.TYPE_WIFI);

            if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
                checkIp();
            }
        }
    };


    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        try {
            Fragment fragment = fragmentList.get(fragmentList.size() - 1);

            if (fragment instanceof ChannelsFragment && isIranian) {
                System.exit(0);
            } else if (fragment instanceof SatFragment ||
                    fragment instanceof RadioFragment ||
                    fragment instanceof TvFragment) {
                System.exit(0);
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } catch (Exception e) {
            System.exit(0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(categoryChangeReceiver);
        unregisterReceiver(radioChannelChangeReceiver);
        unregisterReceiver(internetConnectionReciever);
        unregisterReceiver(vpnReceiver);
    }

    @Override
    public void onGetFavChannels(PFavChannelsAdapter presenterAdapter) {
        rclFavs.setHasFixedSize(true);
        rclFavs.setItemViewCacheSize(20);
        rclFavs.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        //set adapter
        rclFavs.setAdapter(new FavChannelAdapter(presenterAdapter));
    }
}

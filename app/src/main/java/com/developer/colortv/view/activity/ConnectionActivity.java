package com.developer.colortv.view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;

import com.developer.colortv.ui.dialog.CustomDialogClass;

public class ConnectionActivity extends AppCompatActivity {

    private boolean isRecieverRegistered = false, isNetDialogShowing = false;

    public boolean state;
    public CustomDialogClass Aldialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerReceiver(internetConnectionReciever, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }


    public BroadcastReceiver internetConnectionReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo activeWIFIInfo = connectivityManager.getNetworkInfo(connectivityManager.TYPE_WIFI);

            if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
                removeInternetDialog();
            } else {
                if (isNetDialogShowing) {
                    return;
                }
                showInternetDialog();
            }
        }
    };


    private void showInternetDialog() {

        isNetDialogShowing = true;
        state = true;
        Aldialog = new CustomDialogClass(this, "", 0);
        Aldialog.setCancelable(false);
        Aldialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    if (Aldialog != null && Aldialog.isShowing()) {
                        Aldialog.dismiss();
                    }
                }
                return true;
            }
        });

        Aldialog.show();

    }

    private void removeInternetDialog() {


        if (Aldialog != null && Aldialog.isShowing()) {
            state = false;
            Aldialog.dismiss();

            isNetDialogShowing = false;
            Aldialog = null;
        }
    }

    @Override
    protected void onResume() {
//        if (!isRecieverRegistered) {
        registerReceiver(internetConnectionReciever, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
//            isRecieverRegistered = true;
//        }

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager.getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            removeInternetDialog();
        } else {
            if (isNetDialogShowing) try {
                showInternetDialog();
            } catch (Exception e) {
            }

        }
        super.onResume();
    }


////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy() {
        if (isRecieverRegistered) {
            unregisterReceiver(internetConnectionReciever);
            isRecieverRegistered = false;
        }

        super.onDestroy();
    }
}

package com.developer.colortv.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.developer.colortv.R;
import com.developer.colortv.model.data_model.Channel;
import com.developer.colortv.presenter.adapter.PSearchAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    PSearchAdapter presenter;
    boolean firstTime;

    public SearchAdapter(PSearchAdapter presenter, boolean firstTime) {
        this.presenter = presenter;
        this.firstTime = firstTime;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
//        if (firstTime) {
//            View view1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_history, viewGroup, false);
//            return new viewHolderHistory(view1);
//        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_search, viewGroup, false);
            return new viewHolderSearch(view);
//        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
//        if (!firstTime) {
            //show search list
            presenter.onBindSearchItems((viewHolderSearch)viewHolder, i);
//        } else {
//            //show history
//            presenter.onBindHistoryItem((viewHolderHistory)viewHolder, i);
//        }

    }

    @Override
    public int getItemCount() {
//        if (firstTime)
//            return presenter.getHistoryItemCount();
//        else
            return presenter.getSearchItemCount();
    }

    public class viewHolderSearch extends RecyclerView.ViewHolder {
        public TextView txtName, txtType;
        public ImageView img;
        public RelativeLayout rlSearchItem;

        public viewHolderSearch(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.search_item_name);
            txtType = itemView.findViewById(R.id.search_item_type);
            img = itemView.findViewById(R.id.search_item_icon);
            rlSearchItem = itemView.findViewById(R.id.rlSearchItem);
        }
    }

    public class viewHolderHistory extends RecyclerView.ViewHolder {
        public TextView txtName, txtType;
        public ImageView btnRemoveFromHistory;
        public LinearLayout llHistoryItem;

        public viewHolderHistory(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txtHistoryItemName);
            txtType = itemView.findViewById(R.id.txtHistoryItemType);
            btnRemoveFromHistory = itemView.findViewById(R.id.btnRemoveFromHistory);
            llHistoryItem = itemView.findViewById(R.id.llHistoryItem);
        }
    }

    public interface setOnItemClickListener {
        void onClick(Channel channel);
    }
}